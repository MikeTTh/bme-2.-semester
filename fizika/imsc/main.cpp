#include <SDL2/SDL.h>
#include <SDL2_gfxPrimitives.h>
#include <stdexcept>
#include <cmath>
#include <iostream>
#include <sstream>
#include <vector>

const double unit = 5;
const SDL_Color red = {255, 0, 0, 255};
const SDL_Color blue = {0, 0, 255, 255};
const SDL_Color white = {255, 255, 255, 255};
const double k = 1.380649e-23;
const double eps = 0.035;
const int n = 20;

struct Point {
    double x, y;
    explicit Point(double x = 0, double y = 0): x(x), y(y) {}
    double distance(const Point& p) {
        double xdist = p.x-x;
        double ydist = p.y-y;
        return sqrt(xdist*xdist + ydist*ydist);
    }
    Point midpoint(const Point& p) {
        return Point((p.x+x)/2, (p.y+y)/2);
    }
};

struct Charge: public Point {
    double charge;
    explicit Charge(Point p = Point(), double charge = 0): Point(p), charge(charge) {}
    void draw(SDL_Renderer* r) {
        SDL_Color c = (charge < 0) ? blue : red;
        filledCircleRGBA(r, x, y, unit, c.r, c.g, c.b, c.a);
        std::stringstream s;
        s << "Q=" << charge << "C";
        stringRGBA(r, x+unit, y, s.str().c_str(), c.r, c.g, c.b, c.a);
    }
};

double getPot(Point p, Charge q1, Charge q2) {
    double pot1 = k*q1.charge/p.distance(q1);
    double pot2 = k*q2.charge/p.distance(q2);
    return pot1 + pot2;
}

Uint32 timer(Uint32 interv, void*) {
    SDL_Event ev;
    ev.type = SDL_USEREVENT;
    SDL_PushEvent(&ev);
    return interv;
}

int main() {
    if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
        throw std::logic_error(SDL_GetError());
    
    
    SDL_DisplayMode dm;
    SDL_GetCurrentDisplayMode(0, &dm);
    SDL_Window* window = SDL_CreateWindow("Fizika", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, dm.w, dm.h, SDL_WINDOW_FULLSCREEN_DESKTOP);
    
    if (window == NULL)
        throw std::logic_error(SDL_GetError());
    
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL)
        throw std::logic_error(SDL_GetError());
    
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);
    
    Charge q1(Point(dm.w/3, dm.h/2), 0.5), q2(Point(2*dm.w/3, dm.h/2), -1);
    
    SDL_TimerID id = SDL_AddTimer(5000, timer, nullptr);
    
    SDL_Event ev;
    
    SDL_Delay(10);
    stringRGBA(renderer, dm.w/2, dm.h/2, "Calculating...", white.r, white.g, white.b, white.a);
    SDL_RenderPresent(renderer);
    SDL_Delay(10);
    
    timer(0, nullptr);
    
    while (SDL_WaitEvent(&ev)) {
        switch(ev.type) {
            case SDL_KEYDOWN:
                switch (ev.key.keysym.scancode){
                    case SDL_SCANCODE_Q:
                        goto done;
                        break;
                }
                break;
            case SDL_QUIT:
                goto done;
                break;
            case SDL_USEREVENT:
                SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
                SDL_RenderClear(renderer);
                
                double closestTo0 = 99999;
                double x0 = dm.w/2;
                
                std::vector<double> pots;
                double interv = (q2.x - q1.x)/n;
                for (double x = q1.x; x < q2.x; x += interv) {
                    double pot = getPot(Point(x, q1.y), q1, q2);
                    pots.push_back(pot);
                    if (fabs(pot) < closestTo0) {
                        closestTo0 = fabs(pot);
                        x0 = x;
                    }
                }
                pots.push_back(0);
                double maxDist = Point(0,0).distance(Point(x0, (double)dm.h/2));
        
                lineRGBA(renderer, q1.x, q1.y, q2.x, q2.y, white.r, white.g, white.b, white.a);
                
                for(double& cur: pots) {
                    for (int x = 0; x < dm.w; ++x) {
                        for (int y = 0; y < dm.h; ++y) {
                            double dist = Point(x0, dm.h/2).distance(Point(x, y));
                            double e = eps - dist/maxDist * eps/1.1;
                            double curPot = getPot(Point(x, y), q1, q2);
                            double ratio = curPot/cur;
                            if (cur == 0) {
                                ratio = fabs(curPot)*1e27+1; // TODO: dinamikus nagy szám
                            }
                            if (ratio <= 1.0+e && ratio >= 1.0-e) {
                                SDL_Color c;
                                if (cur == 0) {
                                    c = white;
                                } else if (cur < 0) {
                                    c = blue;
                                } else {
                                    c = red;
                                }
                                SDL_SetRenderDrawColor(renderer, c.r, c.g, c.b, c.a);
                                SDL_RenderDrawPoint(renderer, x, y);
                            }
            
                        }
                    }
                }
        
                std::stringstream ss;
                ss << "d=" << q2.x - q1.x << "m";
                stringRGBA(renderer, dm.w/2, dm.h/2, ss.str().c_str(), white.r, white.g, white.b, white.a);
                
                SDL_Color tc = white;
                for (auto& cur: pots) {
                    for (int y = 0; y < dm.h; ++y) {
                        for (int x = 0; x < dm.w; ++x) {
                            double dist = Point(x0, dm.h/2).distance(Point(x, y));
                            double e = eps - dist/maxDist * eps/1.1;
                            double curPot = getPot(Point(x, y), q1, q2);
                            double ratio = curPot/cur;
                            if (cur == 0) {
                                ratio = fabs(curPot)*1e27+1;
                            }
                            if (ratio <= 1.0+e && ratio >= 1.0-e) {
                                std::stringstream ss;
                                ss << "U=" << cur << "V";
                                stringRGBA(renderer, x, y, ss.str().c_str(), tc.r, tc.g, tc.b, tc.a);
                                goto titleDone;
                            }
                        }
                    }
                    titleDone:;
                }
                
                q1.draw(renderer);
                q2.draw(renderer);
        
                SDL_RenderPresent(renderer);
                break;
        }
    }
    done:
    SDL_RemoveTimer(id);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    
    return 0;
}
