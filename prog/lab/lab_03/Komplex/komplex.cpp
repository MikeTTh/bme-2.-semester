/**
 * \file komplex.cpp
 *
 * Komplex osztályt megvalósító függvények definíciója.
 *
 * Folytassa a megvalósítást sorrendben a komplex.h fájlban levő feladatoknak megfelelően!
 *
 */


#include <iostream>         // Valószínű, hogy a kiíráshoz majd kell
#include <iomanip>          // ... és ez is.
#include <cmath>            // az sqrt miatt kell.

#include "komplex.h"        // Ebben van a Komplex osztály, és néhány globális függvény deklarációja

///using namespace std;  // ha nagyon kell, akkor csak itt nyissuk ki a névteret, a headerben soha!

/// Előre elkészített tagfüggvények

/// 0, 1 és 2 paraméteres konstruktor
/// Figyelje meg, hogy a default argumentumokat CSAK a deklarációnál
/// adtuk meg!
Komplex::Komplex(double r, double im) {
    re = r; // ha nincs névütközés, nem kell kiírni a this-t
    this->im = im;
}

///---- Egy példa a konstans tagfüggvényre, ehhez hasonlóan kell
///     elkészíteni a gettereket az 1. feladatban (ELKESZULT=1)
/// Abszolút érték lekérdezése
/// @return - abszolút érték
double Komplex::abs() const { return sqrt(re*re + im*im); }

double Komplex::getRe() const { return re; }
double Komplex::getIm() const { return im; }
void Komplex::setRe(double re) { this->re = re; }
void Komplex::setIm(double im) { this->im = im; }

#if ELKESZULT >= 3
// 3. feladathoz (ELKESZULT 3)
// összehasonlítás

bool Komplex::operator==(const Komplex &rhs_k) const {
    return getRe() == rhs_k.getRe() && getIm() == rhs_k.getIm();
}

/// Egyenlőtlenség vizsgálat
/// @param rhs_k - jobb oldali operandus (Komplex)
/// @return hamis - ha a képzetes és a valós rész is azonos, egyébként false
bool Komplex::operator!=(const Komplex& rhs_k) const {  // visszavezetjük az egyenlőség vizsgálatra
    return !(*this == rhs_k);
}
#endif

#if ELKESZULT >= 4

Komplex Komplex::operator+(const Komplex &rhs_k) const {
    double real = getRe() + rhs_k.getRe();
    double imag = getIm() + rhs_k.getIm();
    return Komplex(real, imag);
}

Komplex Komplex::operator+(double rhs_d) const {
    double real = getRe() + rhs_d;
    double imag = getIm();
    return Komplex(real, imag);
}

#endif

Komplex operator+(double lhs_d, const Komplex& rhs_k){
    double real = lhs_d + rhs_k.getRe();
    return Komplex(real, rhs_k.getIm());
}

#if ELKESZULT >= 6
// 6. feladathoz (ELKESZULT 6)
// a += művelet viszont módosítja önmagát!
Komplex& Komplex::operator+=(const Komplex &rhs_k) {
    re += rhs_k.getRe();
    im += rhs_k.getIm();
    return *this;
}

/// Komplex + double
/// @param rhs_d - jobb oldali operandus (double)
/// @return eredeti (bal oldali) objektum ref., amihez hozzáadtuk rhd_d-t
Komplex& Komplex::operator+=(double rhs_d) {
    re += rhs_d;
    return *this;
}
#endif

std::ostream& operator<<(std::ostream& os, const Komplex& rhs_k){
    os << rhs_k.getRe() << (rhs_k.getIm()>=0?"+":"") << rhs_k.getIm() << "j";
    return os;
}

std::istream& operator>>(std::istream& is, Komplex& rhs_k) {
    char c, tmp;
    double real, imag;
    is >> real >> c >> imag >> tmp;
    
    switch (c){
        case '+':
            break;
        case '-':
            imag = -imag;
            break;
        default:
            throw "Hibás formátum";
            break;
    }
    
    rhs_k.setRe(real);
    rhs_k.setIm(imag);
    
    return is;
}

Komplex Komplex::operator~() const {
    return Komplex(getRe(), -getIm());
}

Komplex Komplex::operator*(const Komplex &rhs_k) const {
    double r = abs() * rhs_k.abs();
    
    double lalpha = atan(getIm()/getRe());
    double ralpha = atan(rhs_k.getIm()/rhs_k.getRe());
    double alpha = lalpha+ralpha;
    
    return Komplex(r*cos(alpha), r*sin(alpha));
}

Komplex Komplex::operator*(double rhs_d) const {
    return (*this) * Komplex(rhs_d);
}

Komplex & Komplex::operator*=(const Komplex &rhs_k) {
    *this = *this * rhs_k;
    return *this;
}

Komplex & Komplex::operator*=(double rhs_d) {
    *this = *this * rhs_d;
    return *this;
}

// ------------------------ Eddig elkészítettük -------------------------
// TODO: A hiányzó tag- és globális függvények itt következnek

