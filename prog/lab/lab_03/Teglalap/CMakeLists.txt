cmake_minimum_required(VERSION 3.15)
project(Teglalap)

set(CMAKE_CXX_STANDARD 14)

add_executable(Teglalap teglalap.cpp)