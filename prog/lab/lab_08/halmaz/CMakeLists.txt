cmake_minimum_required(VERSION 3.15)
project(halmaz)

set(CMAKE_CXX_STANDARD 98)

add_executable(halmaz halmaz_teszt.cpp myset.hpp)