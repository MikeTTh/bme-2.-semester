#ifndef MYSET_HPP
#define MYSET_HPP

#define ELKESZULT 5

template<typename T>
bool equal(const T& a, const T& b){
    return a == b;
}

bool Xequal(const Point& a, const Point& b) {
    return a.getX() == b.getX();
}

bool operator==(const Point& a, const Point& b) {
    return (a.getX() == b.getX()) && (a.getY() == b.getY());
}

template<typename T, size_t N = 10, bool (*cmp)(const T& a, const T& b) = equal>
class Set {
    size_t num;
    T array[N];
public:
    Set(): num(0) {}
    size_t size() {
        return num;
    }
    bool isElement(T elem) {
        for (size_t i = 0; i < num; ++i) {
            if (cmp(elem, array[i])) {
                return true;
            }
        }
        return false;
    }
    void insert(T elem){
        if (num>=N)
            throw "hiba";
        
        if (isElement(elem))
            return;
        
        array[num++] = elem;
    }
};

#endif