#ifndef ALAPTIPUS_HPP
#define ALAPTIPUS_HPP

template<typename T>
class Alaptipus {
    T adat;
public:
    Alaptipus(T i = 0): adat(i) {}
    operator T&() { return adat; }
    operator T() const { return adat; }
    T* operator&() { return &adat; }
    const int* operator&() const { return &adat; }
    virtual ~Alaptipus() {}
};

template<typename T>
void kiir(T* first, T* last, std::ostream& os = std::cout) {
    while (first != last)
        os << *first++ << ' ';
    os << std::endl;
}

#endif