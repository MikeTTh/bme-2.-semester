/**
 * \file: mystack.hpp
 *
 */
#include <vector>

template <class T, class Container = std::vector<T> >
class MyStack {
    Container cont;
public:
    /*MyStack(): cont() {}
    ~MyStack(){}*/
    T& top() {
        if (cont.empty())
            throw "buta";
        return cont.back();
    }
    void push(const T& t) {
        cont.push_back(t);
    }
    void pop() {
        cont.pop_back();
    }
    bool empty() {
        return cont.empty();
    }
};
