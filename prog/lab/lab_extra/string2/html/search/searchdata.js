var indexSectionsWithContent =
{
  0: "abcefgnoprst~",
  1: "st",
  2: "g",
  3: "grs",
  4: "abceoprst~",
  5: "fnst",
  6: "efst"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Macros"
};

