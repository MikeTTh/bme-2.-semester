var searchData=
[
  ['operator_2b_60',['operator+',['../classString.html#a28a020f061e5f25317af47488bfb7de8',1,'String::operator+(const String &amp;rhs_s) const'],['../classString.html#abb187e5ab66fe29c42aed4845b55dd76',1,'String::operator+(char rhs_c) const'],['../classString.html#a28a020f061e5f25317af47488bfb7de8',1,'String::operator+(const String &amp;rhs_s) const'],['../classString.html#abb187e5ab66fe29c42aed4845b55dd76',1,'String::operator+(char rhs_c) const'],['../string2_8h.html#a8d89d501f582c3a65df894dbd030bbcf',1,'operator+():&#160;string2.h']]],
  ['operator_3c_3c_61',['operator&lt;&lt;',['../string2_8h.html#aef14876e55f43fb4ffa7b91664467ecb',1,'string2.cpp']]],
  ['operator_3d_62',['operator=',['../classString.html#a803e6112834d4c0cdb0da6c6a4000e6e',1,'String::operator=(const String &amp;rhs_s)'],['../classString.html#a719edc197aa730e210284c6bdf4792a8',1,'String::operator=(const String &amp;rhs_s)']]],
  ['operator_3e_3e_63',['operator&gt;&gt;',['../string2_8h.html#aea8ed420ad5e70828bc42b0fb9b49366',1,'string2.cpp']]],
  ['operator_5b_5d_64',['operator[]',['../classString.html#a1a7c5c39d4dafbbf8516f5058a253f1a',1,'String::operator[](unsigned int idx)'],['../classString.html#aaa249e62641872197679cfc7c66118c0',1,'String::operator[](unsigned int idx) const'],['../classString.html#ac4bbc344b1088cdb95315ca2b2658020',1,'String::operator[](unsigned int idx)'],['../classString.html#a70f26c9c64f0f0308167d7001dc6aafd',1,'String::operator[](unsigned int idx) const']]]
];
