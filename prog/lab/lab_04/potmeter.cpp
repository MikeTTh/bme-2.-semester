/**
 * \file potmeter.cpp
 * 
 * SimPoti osztály nem inline tagfüggvényei.
 *
 */

#include "potmeter.h"

// Itt valósítsa meg a SimPoti osztály tagfüggvényeit!

SimPoti::SimPoti(double r) {
    R = r;
    pos = 50;
}

int SimPoti::getPos() const {
    return pos;
}

int SimPoti::operator++() {
    if (this->pos < 99)
        return ++(this->pos);
    return this->pos;
}

int SimPoti::operator--() {
    if (this->pos > 1)
        return (this->pos)--;
    return this->pos;
}

Resistor SimPoti::getRes() const {
    Resistor r1(R*(double(pos)/100.0));
    Resistor r2(R*(double(100-pos)/100.0));
    
    return r1%r2;
}
