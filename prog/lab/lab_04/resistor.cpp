﻿/**
 * \file resistor.cpp
 *   (UTF-8 kodolasu fajl. Allitsa at a megjenetes kodolasat,
 *    ha a tovabbi kommentek nem olvashatok helyesen!)
 *
 * Ohmikus ellenállást modellező osztály megvalósítása
 */

/**
 * Itt kell megvalósítani a resistor.h-ban deklarált nem inline függvényeket.
 * A Jportára ezt a fájlt kell feltölteni.
 */

#include "resistor.h"
#include <limits>
#include <cmath>
#include <iostream>

double Resistor::defR = 14;

void Resistor::setDef(double r) {
    defR = r;
}

Resistor::Resistor() {
    Pr("ctor0");
    R = defR;
}

Resistor::Resistor(double r) {
    Pr("ctor1");
    R = r;
}

Resistor::Resistor(const Resistor &rhs){
    *this = Resistor(rhs.getR());
}

Resistor::~Resistor(){
    Pr("dtor");
}

Resistor & Resistor::operator=(const Resistor &rhs) {
    Pr("assign");
    if (this != &rhs) {
        this->R = rhs.getR();
    }
    return *this;
}

double Resistor::getI(double u) const {
    // R=U/I -> I = U/R
    return u/this->getR();
}

double Resistor::getU(double i) const {
    // R=U/I -> U = I*R
    return i*(this->getR());
}

bool Resistor::operator==(const Resistor &r) const {
    double eps = 10 * std::numeric_limits<double>::epsilon(); // 10-szer a legkisebb érték
    
    double a = this->getR();
    double b = r.getR();
    
    if(a == b) return true;
    if (fabs(a - b) < eps)
        return true;
    double aa = fabs(a);
    double ba = fabs(b);
    if (aa < ba) {
        aa = ba;
        ba = fabs(a);
    }
    return (aa - ba) < aa * eps;
}

std::ostream& operator<<(std::ostream &lhs, const Resistor &r){
    lhs << r.getR();
    return lhs;
}

Resistor Resistor::operator+(const Resistor &r) const {
    return Resistor(R + r.R);
}

Resistor Resistor::operator%(const Resistor &r) const {
    return Resistor(1.0/(1.0/R + 1.0/r.R));
}

Resistor operator*(int n, const Resistor& r){
    if (n <= 0) throw "FLGIIG";
    return Resistor(n * r.getR());
}