/**
 * \file pirodistak.h
 * Minden származtatott osztály deklarációját ebbe a fájlba tettük.
 * Az egyszerűség kedvéért minden tagfüggvényt inline valósítottunk meg.
 *
 */
#ifndef PIRODISTAK_H
#define PIRODISTAK_H

#include <stdexcept>
#include "palkalmazott.h"
#include "irodistak.h"

/**
 * Perzisztens csoportvezető
 */
struct PCsopVez : public CsopVez, public Serializable {
    PCsopVez(const std::string& n, double f, csop_t cs)
            : Alkalmazott(n, f),    /// öröklési lánc végén explicit meg kell hívni
              CsopVez(n, f, cs) {}
    /// konverzió miatt kell
    PCsopVez(const CsopVez& csv) : Alkalmazott(csv), CsopVez(csv) {}
    
    void write(std::ostream& os) const {
        os << "PCsopVez" << std::endl;
        PAlkalmazott(*this).write(os);  /// kihasználjuk, hogy kompatibilis
        os << getCs() << std::endl;
    }
    
    void read(std::istream& is) {
        std::string tmp;
        (is >> tmp).ignore(1);
        if (tmp != "PCsopVez")          /// ellenőrizzük, hogy mit olvasunk
            throw std::out_of_range("PCsopvez_R "+tmp);
        PAlkalmazott pa("",0);          /// ebbe olvasunk
        pa.read(is);
        *((Alkalmazott*)this) = pa;     /// kompatibilitás miatt
        csop_t cs;
        (is >> cs).ignore(1);           /// csoport beolvasása
        setCs(cs);
    }
};

/**
 * Perzisztens Határozott idejű alkalmazott
 */
struct PHatIdeju : public HatIdeju, public Serializable {
    PHatIdeju(const std::string& n, double f, time_t t)
            : Alkalmazott(n, f),        /// öröklési lánc végén explicit meg kell hívni
              HatIdeju(n, f, t) {}
    /// konverzió miatt kell
    PHatIdeju(const HatIdeju& hati) : Alkalmazott(hati), HatIdeju(hati) {}
    
    void write(std::ostream& os) const {
        os << "PHatIdeju" << std::endl;
        PAlkalmazott(*this).write(os);  /// kihasználjuk, hogy kompatibilis
        os << getIdo() << std::endl;
    }
    
    void read(std::istream& is) {
        std::string tmp;
        (is >> tmp).ignore(1);
        if (tmp != "PHatIdeju")     /// ellenőrizzük, hogy mit olvasunk
            throw std::out_of_range("PHatIdeju_R "+tmp);
        PAlkalmazott pa("",0);      /// ebbe olvasunk
        pa.read(is);
        *((Alkalmazott*)this) = pa; /// kompatibilitás miatt
        time_t t;
        (is >> t).ignore(1);        /// idő beolvasása
        setIdo(t);
    }
};

/**
 * Perzisztens Határozott idejű csoportvezető
 */
struct PHatIdCsV :public HatIdCsV, public Serializable {
    PHatIdCsV(const std::string& n, double f, csop_t cs, time_t t)
            : Alkalmazott(n, f),        /// öröklési lánc végén explicit meg kell hívni
              HatIdCsV(n, f, cs, t) {}
    
    PHatIdCsV(const HatIdCsV& haticsv) : Alkalmazott(haticsv), HatIdCsV(haticsv) {}
    
    void write(std::ostream& os) const {
        os << "PHatIdCsV" << std::endl;
        PCsopVez(*this).write(os);  /// kihasználjuk, hogy kompatibilis
        os << getIdo() << std::endl;
    }
    
    void read(std::istream& is) {
        std::string tmp;
        (is >> tmp).ignore(1);
        if (tmp != "PHatIdCsV")     /// ellenőrizzük, hogy mit olvasunk
            throw std::out_of_range("PHatIdCsV_R "+tmp);
        PCsopVez pcs("", 0, 0);     /// ebbe olvasunk
        pcs.read(is);
        *((CsopVez*)this) = pcs;    /// kompatibilitás miatt
        time_t t;
        (is >> t).ignore(1);        /// idő beolvasása
        setIdo(t);
    }
};

/**
 * Perzisztens Határozott idejű csoportvezető helyettes
 */
class PHatIdCsVezH :public HatIdCsVezH, public Serializable {
public:
    PHatIdCsVezH(const  std::string& n, double f, time_t t, HatIdCsV& kit)
            : Alkalmazott(n, f),        /// öröklési lánc végén explicit meg kell hívni
              HatIdCsVezH(n, f, t, kit) {}
    
    PHatIdCsVezH(const HatIdCsVezH& h) : Alkalmazott(h), HatIdCsVezH(h) {}
    
    void write(std::ostream& os) const {
        os << "PHatIdCsVezH" << std::endl;  /// kihasználjuk, hogy kompatibilis
        PHatIdCsV(*this).write(os);
    }
    
    void read(std::istream& is) {
        std::string tmp;
        (is >> tmp).ignore(1);
        if (tmp != "PHatIdCsVezH")      /// ellenőrizzük, hogy mit olvasunk
            throw std::out_of_range("PHatIdCsVezH: read");
        PHatIdCsV phcs("", 0, 0, 0);    /// ebbe olvasunk
        phcs.read(is);
        *((HatIdCsV*)this) = phcs;     /// kompatibilitás miatt
    }
};
#endif // PIRODISTAK_H