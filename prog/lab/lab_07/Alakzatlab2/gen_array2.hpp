#ifndef GEN_ARRAY2_HPP
#define GEN_ARRAY2_HPP
/**
 *  \file gen_array2.hpp
 *
 *  Generikus tömb.
 *  Előadáson bemutatott 2. változat (sablon paraméterként kapott fix méretű).
 *    Az osztály definíciója és a tagfüggvények deklarációit
 *    nem tesszük külön fájlba, mivel a sablonok fordításánál
 *    mindkettőnek elérhetőnek kell lennie!
 *    Ezt jelezzük a .hpp kiterjesztéssel.
 */

/// Generikus tömb sablon
/// @param T - adattípus
/// @param s - méret
template <class T, unsigned int s>   // sablon kezdõdik
class Array {               // osztálysablon
    T t[s];                 // elemek tömbje. s teplate parameter
public:
    /// Indexelés.
    /// @param i - index
    /// @return - referencia az adott indexű elemre
    /// @return - hiba esetén kivételt dob
    T& operator[](unsigned int i) {
        if (i >= s) throw "Indexelesi hiba";
        return t[i];
    }
    
    /// Indexelés konstans függvénye.
    /// @param i - index
    /// @return - referencia az adott indexű elemre
    /// @return - hiba esetén kivételt dob
    const T& operator[](unsigned int i) const {
        if (i >= s) throw "Indexelesi hiba (const)";
        return t[i];
    }
};
#endif
