/**
 *  \file rajztabla.cpp
 *   Rajztábla osztály tagfüggvényinek megvalósítása
 */

#include <stdexcept>
#include "rajztabla.h"

void Rajztabla::mozgat(const Pont& d) {
    for (size_t i = 0; i < db; i++)
        tabla[i]->mozgat(d);
}

void Rajztabla::felrak(Alakzat *alakzat) {
    tabla[db++] = alakzat;
}

void Rajztabla::rajzol() {
    for (int i = 0; i < db; ++i) {
        tabla[i]->rajzol();
    }
}

void Rajztabla::torol() {
    for (int i = 0; i < db; ++i) {
        delete tabla[i];
    }
    db = 0;
}

Rajztabla::Rajztabla(const Rajztabla& r) {
    db = 0;
    for (int i = 0; i < r.getDb(); ++i) {
        felrak(r[i]->clone());
    }
}

Rajztabla& Rajztabla::operator=(const Rajztabla &r) {
    if (this == &r) {
        return *this;
    }
    this->torol();
    db = 0;
    for (int i = 0; i < r.getDb(); ++i) {
        Alakzat* a = r[i];
        felrak(a->clone());
    }
}