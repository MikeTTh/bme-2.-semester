/**
 *  \file rajztabla.h
 *  Rajztabla osztály deklarációja
 */

#ifndef RAJZTABLA_H
#define RAJZTABLA_H

#include "alakzat.h"
#include "gen_array2.hpp"

class Rajztabla {
    static const size_t max = 100;
    //Alakzat* tabla[max];
    Array<Alakzat*, max> tabla; // Ez miben jobb, mint a sima array? Csak simán a reimplementáció + 2 throw, nem?
    size_t db;
public:
    Rajztabla(const Rajztabla&);
    Rajztabla& operator=(const Rajztabla&);
    Rajztabla() :db(0) {}
    void felrak(Alakzat* alakzat);
    void rajzol();
    void mozgat(const Pont& d);
    void torol();
    
    int getDb() const {
        return db;
    }
    
    Alakzat* operator[](int i) const {
        if (i >= db)
            throw std::out_of_range("Túlindexelés");
        return tabla[i];
    }
    
    virtual ~Rajztabla() {torol();}
};

#endif // RAJZTABLA_H
