/**
 *
 * \file string5.cpp
 *
 * Itt kell megvalósítania a hiányzó tagfüggvényeket.
 * Segítségül megadtuk a C++ nem OO eszközeinek felhasználásával készített String-kezelő
 * függvényke neveit.
 *
 * Ha valamit INLINE-ként valósít meg, akkor annak a string5.h-ba kell kerülnie,
 * akár kívül akár osztályon belül definiálja. (Az inline függvényeknek minden
 * fordítási egységben elérhetőknek kell lenniük)
 * *
 * A teszteléskor ne felejtse el beállítani a string5.h állományban az ELKESZULT makrót.
 *
 */

#ifdef _MSC_VER
// MSC ne adjon figyelmeztető üzenetet a C sztringkezelő függvényeire
  #define _CRT_SECURE_NO_WARNINGS
#endif

#include <iostream>             // Kiíratáshoz
#include <cstring>              // Sztringműveletekhez

#include "memtrace.h"           // a standard headerek után kell lennie
#include "string5.h"


/// Konstruktorok: egy char karakterből (createString)
///                egy nullával lezárt char sorozatból (createString)

String::String() :len(0) {
    pData = new char[1];
    pData[0] = '\0';
}

String::String(const char c) :len(1){
    pData = new char[2];
    pData[0] = c;
    pData[1] = '\0';
}

String::String(const char* const str){
    len = strlen(str);
    pData = new char[len+1];
    strcpy(pData, str);
}

/// Másoló konstruktor: String-ből készít (createString)
String::String(const String& s){
    len = s.size();
    pData = new char[len+1];
    strcpy(pData, s.c_str());
}

/// Destruktor (disposeString)
String::~String() {
    delete[] pData;
}

/// operator=

String& String::operator=(const String &s) {
    if (this == &s)
        return *this;
    delete[] pData;
    len = s.size();
    pData = new char[len+1];
    strcpy(pData, s.c_str());
    return *this;
}


/// [] operátorok: egy megadott indexű elem REFERENCIÁJÁVAL térnek vissza (charAtString)
/// indexhiba esetén const char * kivételt dob!
char& String::operator[](size_t i) {
    if (i >= size()) throw "FLGIIG";
    return pData[i];
}

const char& String::operator[](size_t i) const {
    if (i >= size()) throw "FLGIIG";
    return pData[i];
}

/// + operátorok:
///                 String-hez jobbról karaktert ad (addString)
///                 String-hez String-et ad (addString)
String String::operator+(const String& s) const {
    char* c = new char[this->size() + s.size() + 1];
    strcpy(c, this->c_str());
    strcat(c, s.c_str());
    String str(c);
    delete[] c;
    return str;
}

String String::operator+(const char& c) const {
    char* s = new char[this->size() + 2];
    strcpy(s, this->c_str());
    s[this->size()] = c;
    s[this->size()+1] = '\0';
    String str(s);
    delete[] s;
    return str;
}

String operator+(const char& c, const String& s){
    char* cs = new char[s.size() + 2];
    cs[0] = c;
    cs[1] = '\0';
    strcat(cs, s.c_str());
    String str(cs);
    delete[] cs;
    return str;
}

/// << operator, ami kiír az ostream-re
std::ostream& operator<<(std::ostream& os, const String& s){
    return os << s.c_str();
}

/// >> operátor, ami beolvas az istream-ről egy szót
std::istream& operator>>(std::istream& is, String& s){
    char c;
    while(is.get(c)) if (!isspace(c)) break;
    s = s+c;
    while(is.get(c)){
        if (isspace(c))
            return is;
        s = s + c;
    }
    return is;
}