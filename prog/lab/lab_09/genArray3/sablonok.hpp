#ifndef SABLONOK_HPP
#define SABLONOK_HPP

#include <iostream>

#define ELKESZULT 18

template<typename T>
void printEach(T from, T to, std::ostream& os = std::cout, const char* format = ", ") {
    for (T mozgo = from; mozgo != to;) {
        os << *mozgo;
        ++mozgo;
        if (mozgo != to)
            os << format;
    }
    os << std::endl;
}

template<typename T>
int szamol_ha_negativ(T from, T to) {
    int neg = 0;
    for (T mozgo = from; mozgo != to; ++mozgo) {
        if (*mozgo < 0)
            neg++;
    }
    return neg;
}

template<typename T>
class nagyobb_mint {
    T ref;
public:
    nagyobb_mint(T r): ref(r) {}
    
    bool operator()(T cmp){
        return cmp > ref;
    }
};

template<typename T, typename F>
int szamol_ha(T from, T to, F f) {
    int ret = 0;
    for (T mozgo = from; mozgo != to; ++mozgo) {
        if (f(*mozgo))
            ret++;
    }
    return ret;
}


/// Függvénysablon, ami kiírja egy generikus tömb adatait
/// Feltételezzük, hogy a generikus tömbnek van:
///   - ForwardIteratora, size(), valamint capacity() tagfüggvénye
/// @param T   - sablon paraméter: iterátoros tömb típus
/// @param txt - kiírandó szöveg
/// @param arr - konkrét tömb
/// @param os  - output steram
template <class T>
void PrintArray(const char *txt, const T& arr, std::ostream& os = std::cout) {
    os << txt << " size=" << arr.size()
       << " capacity=" << arr.capacity() << std::endl;
    os << "\t data=";
    for (typename T::iterator i = arr.begin(); i != arr.end();) {
        os << *i;
        ++i;
        if (i != arr.end()){
            os << ",";
        }
    }
    os << std::endl;
    
}

template <typename InputIterator, class Func>
Func forEach(InputIterator first, InputIterator last, Func func) {
    for (InputIterator i = first; i != last; i++) {
        func(*i);
    }
    return func;
}

template<typename T>
class ostreamFunctor {
    std::ostream& os;
    const char* sep;
public:
    ostreamFunctor(std::ostream& o, const char* s): os(o), sep(s) {}
    void operator()(T data) {
        os << data << sep;
    }
};

#endif