#ifndef SABLONOK_HPP
#define SABLONOK_HPP

#include <iostream>

#define ELKESZULT 7

template<typename T>
void printEach(T from, T to, std::ostream& os = std::cout, const char* format = ", ") {
    for (T mozgo = from; mozgo != to;) {
        os << *mozgo;
        ++mozgo;
        if (mozgo != to)
            os << format;
    }
    os << std::endl;
}

template<typename T>
int szamol_ha_negativ(T from, T to) {
    int neg = 0;
    for (T mozgo = from; mozgo != to; ++mozgo) {
        if (*mozgo < 0)
            neg++;
    }
    return neg;
}

template<typename T>
class nagyobb_mint {
    T ref;
public:
    nagyobb_mint(T r): ref(r) {}
    
    bool operator()(T cmp){
        return cmp > ref;
    }
};

template<typename T, typename F>
int szamol_ha(T from, T to, F f) {
    int ret = 0;
    for (T mozgo = from; mozgo != to; ++mozgo) {
        if (f(*mozgo))
            ret++;
    }
    return ret;
}

#endif