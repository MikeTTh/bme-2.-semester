#include <cstring>
#include <cctype>
#include "nem_oo.h"

/**
 * \file nem_oo_teszt.cpp
 * (UTF-8 kodolasu fajl. Allitsa at a kodolast,
 *  ha a tovabbi kommentek nem olvashatok helyesen!)
 *
 * FELADATOK:
 *  1. Ebben a fájlban valósítsa meg a nem_oo.h állományban deklarált függvényeket!
 *
 * Ügyeljen a helyes névtér használatra!
 *   Tipp: Használja a scope operátort, vagy nyisson névteret!
 */

int sajat::atoi(const char *p, int base) {
    int szam = 0;
    int hely = 0;
    while(p[hely] != '\0' || isspace(p[hely])){
        int tmp = 0;
        if (p[hely] >= '0' && p[hely] <= '9') {
            tmp = p[hely] - '0';
        } else if (p[hely] >= 'a' && p[hely] <= 'z'){
            tmp = p[hely] - 'a' + 10;
        } else if (p[hely] >= 'A' && p[hely] <= 'Z'){
            tmp = p[hely] - 'A' + 10;
        } else {
            throw "FLGIIG";
        }
        if (tmp >= base)
            throw "FLGIIG";
        szam *= base;
        szam += tmp;
        
        hely++;
    }
    return szam;
}

char* sajat::strcat(const char *p1, const char *p2) {
    size_t hossz = strlen(p1) + strlen(p2) + 1;
    char* ret = new char[hossz];
    size_t i;
    for (i = 0; i <= strlen(p1); ++i) {
        ret[i] = p1[i];
    }
    for (size_t j = 0; j <= strlen(p2); ++j) {
        ret[i+j-1] = p2[j];
    }
    ret[hossz-1] = '\0';
    return ret;
}

char* sajat::unique(char *first, char *last) {
    int regi_hely = 0;
    int uj_hely = 0;
    while (&first[regi_hely] <= last) {
        if (first[regi_hely] != first[uj_hely]) {
            uj_hely++;
            first[uj_hely] = first[regi_hely];
        }
        regi_hely++;
    }
    return &first[uj_hely];
}