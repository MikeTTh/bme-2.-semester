//
// Created by mike on 14/03/2020.
//

#ifndef CPPEMBER_EMBER_H
#define CPPEMBER_EMBER_H

class Ember{
    char* nev;
public:
    int szulEv;
    Ember(const char* n = "FLGIIG", int ev = 2000);
    Ember(const Ember& e);
    Ember& operator=(const Ember& e);
    const char* getNev() const;
    
    virtual int getKor(int ev) const;
    virtual const char* foglalkozas() const;
    virtual ~Ember();
};


#endif //CPPEMBER_EMBER_H
