#include "vektor.h"
#include "memtrace.h"

size_t Vektor::defSize = 24;
double Vektor::defValue = 14;


Vektor::Vektor(const Vektor &v) {
    nElements = v.size();
    pVec = new double[nElements];
    for (size_t i = 0; i < nElements; ++i) {
        pVec[i] = v[i];
    }
}

Vektor::~Vektor() {
    delete[] pVec;
}

Vektor& Vektor::operator=(const Vektor &v) {
    if (this == &v)
        return *this;
    
    delete[] pVec;
    
    nElements = v.size();
    pVec = new double[nElements];
    for (size_t i = 0; i < nElements; ++i) {
        pVec[i] = v[i];
    }
    return *this;
}

double& Vektor::operator[](size_t idx) {
    if (idx >= nElements)
        throw "FLGIIG";
    
    return pVec[idx];
}

const double& Vektor::operator[](size_t idx) const {
    if (idx >= nElements)
        throw "FLGIIG";
    
    return pVec[idx];
}

Vektor operator*(double val, const Vektor& vec) {
    Vektor v(vec);
    for (size_t i = 0; i < v.size(); ++i) {
        v[i] *= val;
    }
    return v;
}