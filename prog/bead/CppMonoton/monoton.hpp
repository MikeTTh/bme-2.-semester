#ifndef MONOTON_HPP
#define MONOTON_HPP

template<typename Iterator, typename func>
bool monoton(Iterator begin, Iterator end, func pred){
    for (Iterator i = begin; i != end;) {
        Iterator p = i;
        i++;
        if (i != end && !pred(*i, *p))
            return false;
    }
    return true;
}

#endif