//
// Created by mike on 30/03/2020.
//

#ifndef CPPBOLT_KIFLI_H
#define CPPBOLT_KIFLI_H


#include "aru.h"
#include "string5.h"

class Kifli: public Aru {
    String spec;
public:
    Kifli(double ar, const char* spec = ""): Aru("Kifli", "db", ar), spec(spec) {}
    
    std::ostream& print(std::ostream& os) const {
        return os << megnevezes << " "
                  << spec << "; "
                  << ar << "Ft/" << egyseg;
    }
    
};


#endif //CPPBOLT_KIFLI_H
