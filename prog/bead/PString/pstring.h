//
// Created by mike on 20/04/2020.
//

#ifndef PSTRING_PSTRING_H
#define PSTRING_PSTRING_H

#include "string5.h"
#include "serializable.h"
#include <cstdlib>

class PString: public Serializable, public String {
public:
    PString(const char* s = ""): String(s) {}
    PString(const String& s): String(s) {}
    void write(std::ostream& os) const {
        os << size() << " " << *this;
    }
    void read(std::istream& is) {
        size_t siz = 0;
        is >> siz;
        
        char c;
        is.get(c);
        char* s = new char[siz+1];
        for (size_t i = 0; i < siz; ++i) {
            is.get(c);
            s[i] = c;
        }
        s[siz] = '\0';
        
        *this = PString(s);
        delete[] s;
    }
};


#endif //PSTRING_PSTRING_H
