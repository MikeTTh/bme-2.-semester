#include <iostream>
#include <sstream>
#include <vector>
#include <list>
#include <deque>

#include "gtest_lite.h"
#include "memtrace.h"
#include "testclass.h"
#include "tar.hpp"

#ifndef ELKESZULT
  #define ELKESZULT  0
#endif

int main() {
    GTINIT(std::cin);       // Csak C(J)PORTA működéséhez kell

#if ELKESZULT >= 1
    /// Csak a példányosodást ellenőrizzük
    TEST(Test1, sanity_default) {
        Tar<int*> t1;
        EXPECT_EQ((size_t)0, t1.size()) << "Nem ures?" << std::endl;
        EXPECT_TRUE(t1.empty()) << "Nem ures?" << std::endl;
    } ENDM

    TEST(Test1, sanity_deque) {
        Tar<int*, std::deque<int*> > t1;
        EXPECT_EQ((size_t)0, t1.size()) << "Nem ures?" << std::endl;
        EXPECT_TRUE(t1.empty()) << "Nem ures?" << std::endl;
    } ENDM

    TEST(Test1, sanity_list) {
        Tar<int*, std::list<int*> > t1;
        EXPECT_EQ((size_t)0, t1.size()) << "Nem ures?" << std::endl;
        EXPECT_TRUE(t1.empty()) << "Nem ures?" << std::endl;
    } ENDM

    TEST(Test1, copy_ctor_default) {
        Tar<int*> t1;
        EXPECT_THROW(Tar<int*> t2 = t1, std::exception);
    } ENDM

    TEST(Test1, assign_default) {
        Tar<int*> t1, t2;
        EXPECT_THROW(t2 = t1, std::exception);
    } ENDM

    TEST(Test1, assign_dequeue) {
        Tar<int*, std::deque<int*> > t1, t2;
        EXPECT_THROW(t2 = t1, std::exception);
    } ENDM

    TEST(Test1, assign_dequeue) {
        Tar<int*, std::list<int*> > t1, t2;
        EXPECT_THROW(t2 = t1, std::exception);
    } ENDM


#endif

#if ELKESZULT >= 2
    /// Kicsit megdolgoztatjuk, a méreteket ellenőrizzük
    TEST(Test2, sanity_default) {
        Tar<TestClass*> t1;
        EXPECT_EQ((size_t)0, t1.size()) << "Nem ures?" << std::endl;
        t1.push_back(NULL);
        EXPECT_EQ((size_t)1, t1.size()) << "Nem jo a meret!" << std::endl;
        t1.push_back(NULL);
        EXPECT_EQ((size_t)2, t1.size()) << "Nem jo a meret!" << std::endl;
        t1.clear();
        EXPECT_EQ((size_t)0, t1.size()) << "Nem torlodott?" << std::endl;
        EXPECT_TRUE(t1.empty()) << "Nem ures?" << std::endl;
    } ENDM

    TEST(Test2, sanity_list) {
        Tar<TestClass*, std::list<TestClass*> > t1;
        EXPECT_EQ((size_t)0, t1.size()) << "Nem ures?" << std::endl;
        t1.push_back(NULL);
        EXPECT_EQ((size_t)1, t1.size()) << "Nem jo a meret!" << std::endl;
        t1.push_back(NULL);
        EXPECT_EQ((size_t)2, t1.size()) << "Nem jo a meret!" << std::endl;
        t1.clear();
        EXPECT_EQ((size_t)0, t1.size()) << "Nem torlodott?" << std::endl;
        EXPECT_TRUE(t1.empty())         << "Nem ures?" << std::endl;
    } ENDM
#endif

#if ELKESZULT >= 3
    /// Ellenőrizzük, hogy megszűnéskor törli-e az objektumokat
    TEST(Test3, dtor) {
        Tar<TestClass*> t1;
        EXPECT_EQ((size_t)0, t1.size());
        t1.push_back(new TestClass2);
        EXPECT_EQ((size_t)1, t1.size());
    } ENDMsg("Megszuneskor felszabaditotta a tarolora bizott objektumokat?")

    TEST(Test3, dtor) {
        Tar<TestClass*, std::deque<TestClass*> > t1;
        EXPECT_EQ((size_t)0, t1.size());
        t1.push_back(new TestClass2);
        EXPECT_EQ((size_t)1, t1.size());
    } ENDMsg("Megszuneskor felszabaditotta a tarolora bizott objektumokat?")

    TEST(Test3, dtor) {
        Tar<TestClass*, std::list<TestClass*> > t1;
        EXPECT_EQ((size_t)0, t1.size());
        t1.push_back(new TestClass2);
        EXPECT_EQ((size_t)1, t1.size());
    } ENDMsg("Megszuneskor felszabaditotta a tarolora bizott objektumokat?")

    /// Ellenőrizzük az iterátoros konstruktort
    TEST(Test3, iter_ctor) {
        int* pt[10];
        for (int i = 0; i < 10; ++i) pt[i] = new int;
        Tar<int*> t1(pt, pt+10);
        EXPECT_EQ((size_t)10, t1.size());
    } ENDMsg("Megszuneskor felszabaditotta a tarolora bizott objektumokat?")

    /// Ellenőrizzük az 1 paraméteres konstruktort
    TEST(Test3, param_ctor) {
        Tar<int*> t1(5);
        EXPECT_EQ((size_t)5, t1.size());
    } ENDMsg("Megszuneskor felszabaditotta a tarolora bizott objektumokat?")

    /// Ellenőrizzük a 2 paraméteres konstruktort
    TEST(Test3, param_ctor) {
        Tar<int*> t1(5, (int*)0);
        EXPECT_EQ((size_t)5, t1.size());
    } ENDMsg("Megszuneskor felszabaditotta a tarolora bizott objektumokat?")
#endif

#if ELKESZULT >= 4
    /// Ellenőrizzük, hogy az erase(iterator) törli-e az objektumokat
    TEST(Test4, erase1) {
        Tar<TestClass1*> t1;
        EXPECT_EQ((size_t)0, t1.size());
        t1.push_back(new TestClass1);
        EXPECT_EQ((size_t)1, t1.size());
        t1.push_back(new TestClass1);
        EXPECT_EQ((size_t)2, t1.size());
        t1.push_back(new TestClass1);
        EXPECT_EQ((size_t)3, t1.size());
        t1.erase(t1.begin()+1);
        EXPECT_EQ((size_t)2, t1.size());
        EXPECT_EQ(2, TestClass::get())          << "Nem szuntette meg az objektumot!"  << std::endl;
        EXPECT_EQ(1, (*t1.begin())->getId())    << "Nem jo objektumot szuntettet meg!"  << std::endl;
        EXPECT_EQ(3, (*(t1.begin()+1))->getId())<< "Nem jo objektumot szuntettet meg!"  << std::endl;
        t1.erase(t1.begin());
        EXPECT_EQ((size_t)1, t1.size());
        EXPECT_EQ(1, TestClass::get())       << "Nem szuntette meg az objektumot!"  << std::endl;
        EXPECT_EQ(3, (*t1.begin())->getId()) << "Nem jo objektumot szuntettet meg!"  << std::endl;
        t1.erase(t1.begin());
        EXPECT_EQ((size_t)0, t1.size());
        EXPECT_TRUE(t1.empty())              << "Nem ures?" << std::endl;
        EXPECT_EQ(0, TestClass::get())       << "Nem szuntette meg az objektumot!"  << std::endl;
    } ENDM;
#endif

#if ELKESZULT >= 5
    /// Ellenőrizzük, hogy az erase(iterator1, iterator2) törli-e az objektumokat
    TEST(Test5, erase2) {
        Tar<TestClass1*, std::deque<TestClass1*> > t1;
        EXPECT_EQ((size_t)0, t1.size());
        t1.push_back(new TestClass1);
        EXPECT_EQ((size_t)1, t1.size());
        t1.push_back(new TestClass1);
        EXPECT_EQ((size_t)2, t1.size());
        t1.push_back(new TestClass1);
        EXPECT_EQ((size_t)3, t1.size());
        t1.push_front(new TestClass1);
        EXPECT_EQ((size_t)4, t1.size());
        t1.erase(t1.begin()+1, t1.end());
        EXPECT_EQ((size_t)1, t1.size());
        EXPECT_EQ(1, TestClass::get()) << "Nem szuntette meg az objektumot!"  << std::endl;
        EXPECT_EQ(4, (*t1.begin())->getId()) << "Nem jo objektumot szuntettet meg!"  << std::endl;
        t1.erase(t1.begin(), t1.end());
        EXPECT_EQ((size_t)0, t1.size());
        EXPECT_TRUE(t1.empty())        << "Nem ures?" << std::endl;
        EXPECT_EQ(0, TestClass::get()) << "Nem szuntette meg az objektumot!"  << std::endl;
    } ENDM;
#endif

#if ELKESZULT >= 6
    /// Traverse tesztelése és megnézzük, hogy jó-e a sorrend
    TEST(Test6, traverse) {
        Tar<TestClass*> t1;
        t1.push_back(new TestClass1(1));
        t1.push_back(new TestClass1(2));
        t1.push_back(new TestClass2("Hello"));
        t1.push_back(new TestClass2("World"));
        t1.push_back(new TestClass1(3));
        t1.push_back(new TestClass2("Bye"));
        std::stringstream ss;

        TestClassPrint print(ss);
        t1.traverse(print);
        EXPECT_TRUE(0 != ss.str().size()) << "Nem irt ki semmit! Traverse mukodik?" << std::endl;
        if (ss.str().size() != 0)
            EXPECT_STREQ("1;2;Hello;World;3;Bye;", ss.str().c_str()) <<
                                                    "Valami baj van! Sorrend?" << std::endl;
    } ENDM
#endif

#if ELKESZULT >= 7
    /// pop_back ellenőrzése
    TEST(Test7, pop_back) {
        Tar<TestClass1*> t1;
        t1.push_back(new TestClass1);
        t1.push_back(new TestClass1);
        t1.push_back(new TestClass1);
        t1.pop_back();
        EXPECT_EQ((size_t)2, t1.size());
        EXPECT_EQ(2, TestClass::get())          << "Nem szuntette meg az objektumot!"  << std::endl;
        EXPECT_EQ(2, t1.back()->getId())    << "Nem jo objektumot szuntettet meg!"  << std::endl;
        t1.pop_back();
        EXPECT_EQ((size_t)1, t1.size());
        EXPECT_EQ(1, TestClass::get())          << "Nem szuntette meg az objektumot!"  << std::endl;
        EXPECT_EQ(1, t1.back()->getId())    << "Nem jo objektumot szuntettet meg!"  << std::endl;
        t1.pop_back();
        EXPECT_EQ((size_t)0, t1.size());
        EXPECT_TRUE(t1.empty())              << "Nem ures?" << std::endl;
        EXPECT_EQ(0, TestClass::get())       << "Nem szuntette meg az objektumot!"  << std::endl;
    } ENDM;
#endif

#if ELKESZULT >= 8
    /// Nem pointer típusra először csak a példányosodást ellenőrizzük
    TEST(Test8, non_poi_sanity_default) {
        Tar<int> t1;
        EXPECT_EQ((size_t)0, t1.size()) << "Nem ures?" << std::endl;
        EXPECT_TRUE(t1.empty()) << "Nem ures?" << std::endl;
    } ENDM

    /// Ellenőrizzük az 1 paraméteres konstruktort
    TEST(Test3, param_ctor) {
        Tar<int> t1(5);
        EXPECT_EQ((size_t)5, t1.size());
    } ENDM

    /// Ellenőrizzük a 2 paraméteres konstruktort
    TEST(Test3, param_ctor) {
        Tar<double> t1(5, 34.7);
        EXPECT_EQ((size_t)5, t1.size());
    } ENDMsg("Megszuneskor felszabaditotta a tarolora bizott objektumokat?")
#endif

#if ELKESZULT >= 9
    /// Nem pointer, kicsit megdolgoztatjuk, a méreteket ellenőrizzük
    TEST(Test9, non_poi_clear) {
        Tar<int> t1;
        EXPECT_EQ((size_t)0, t1.size()) << "Nem ures?" << std::endl;
        t1.push_back(1);
        EXPECT_EQ((size_t)1, t1.size()) << "Nem jo a meret!" << std::endl;
        t1.push_back(2);
        EXPECT_EQ((size_t)2, t1.size()) << "Nem jo a meret!" << std::endl;
        t1.clear();
        EXPECT_EQ((size_t)0, t1.size()) << "Nem torlodott?" << std::endl;
        EXPECT_TRUE(t1.empty()) << "Nem ures?" << std::endl;
    } ENDM
#endif

#if ELKESZULT >= 10
    /// Ellenőrizzük, hogy az erase(iterator) törli-e az objektumokat
    TEST(Test10, non_poi_erase1) {
        Tar<int> t1;
        t1.push_back(1);
        t1.push_back(2);
        t1.push_back(3);
        EXPECT_EQ((size_t)3, t1.size());
        t1.erase(t1.begin()+1);
        EXPECT_EQ((size_t)2, t1.size());
        EXPECT_EQ(3, t1.back())    << "Nem jo erteket szuntettet meg!"  << std::endl;
        t1.erase(t1.begin());
        EXPECT_EQ((size_t)1, t1.size());
        EXPECT_EQ(3, t1.back())    << "Nem jo erteket szuntettet meg!"  << std::endl;
        t1.erase(t1.begin());
        EXPECT_EQ((size_t)0, t1.size());
        EXPECT_TRUE(t1.empty())              << "Nem ures?" << std::endl;
    } ENDM;
#endif

#if ELKESZULT >= 11
    /// Ellenőrizzük, hogy az erase(iterator1, iterator2) törli-e az objektumokat
    TEST(Test11, non_poi_erase2) {
        Tar<int> t1;
        t1.push_back(1);
        t1.push_back(2);
        t1.push_back(3);
        t1.push_back(4);
        EXPECT_EQ((size_t)4, t1.size());
        t1.erase(t1.begin()+1, t1.end());
        EXPECT_EQ((size_t)1, t1.size());
        EXPECT_EQ(1, t1.back()) << "Nem jo erteket szuntettet meg!"  << std::endl;
        t1.erase(t1.begin(), t1.end());
        EXPECT_EQ((size_t)0, t1.size());
        EXPECT_TRUE(t1.empty())        << "Nem ures?" << std::endl;
    } ENDM;
#endif

#if ELKESZULT >= 12
    /// Traverse tesztelése és megnézzük, hogy jó-e a sorrend
    TEST(Test12, non_poi_traverse) {
        Tar<TestClass2> t1;
        t1.push_back(TestClass2("Hello"));
        t1.push_back(TestClass2("Hello"));
        t1.push_back(TestClass2("World"));
        t1.push_back(TestClass2("World"));
        t1.push_back(TestClass2("Bye"));
        t1.push_back(TestClass2("Bye"));
        t1.erase(t1.begin());
        t1.erase(t1.begin()+1, t1.begin()+3);
        std::stringstream ss;

        TestClass2Print print(ss);
        t1.traverse(print);
        EXPECT_TRUE(0 != ss.str().size()) << "Nem irt ki semmit! Traverse mukodik?" << std::endl;
        if (ss.str().size() != 0)
            EXPECT_STREQ("Hello;Bye;Bye;", ss.str().c_str()) <<
                                                    "Valami baj van! Sorrend?" << std::endl;
    } ENDM
#endif

#if ELKESZULT >= 13
    /// pop_back ellenőrzése
    TEST(Test13, non_poi_pop_back) {
        Tar<int> t1;
        t1.push_back(1);
        t1.push_back(2);
        t1.push_back(3);
        t1.pop_back();
        EXPECT_EQ((size_t)2, t1.size());
        EXPECT_EQ(2, t1.back())          << "Nem jo erteket szunettet meg"  << std::endl;
        t1.pop_back();
        EXPECT_EQ((size_t)1, t1.size());
        EXPECT_EQ(1, t1.back())    << "Nem jo erteket szuntettet meg!"  << std::endl;
        t1.pop_back();
        EXPECT_EQ((size_t)0, t1.size());
        EXPECT_TRUE(t1.empty())              << "Nem ures?" << std::endl;
    } ENDM;
#endif

#if ELKESZULT >= 14
    /// copy ctor ellenőrzése
    TEST(Test14, non_poi_copy_ctor) {
        Tar<int> t2;
        t2.push_back(1);
        t2.push_back(2);
        t2.push_back(3);
        Tar <int> t1 = t2;
        EXPECT_EQ((size_t)3, t1.size());
        EXPECT_EQ(3, t1.back())          << "Nem jo erteket szunettet meg"  << std::endl;
        t1.pop_back();
        EXPECT_EQ((size_t)2, t1.size());
        EXPECT_EQ(2, t1.back())          << "Nem jo erteket szunettet meg"  << std::endl;
        t1.pop_back();
        EXPECT_EQ((size_t)1, t1.size());
        EXPECT_EQ(1, t1.back())    << "Nem jo erteket szuntettet meg!"  << std::endl;
        t1.pop_back();
        EXPECT_EQ((size_t)0, t1.size());
        EXPECT_TRUE(t1.empty())              << "Nem ures?" << std::endl;
    } ENDM;
#endif

#if ELKESZULT >= 15
    /// assign ellenőrzése
    TEST(Test15, non_poi_assign) {
        Tar<int> t1, t2;
        t2.push_back(1);
        t2.push_back(2);
        t2.push_back(3);
        t1 = t2;
        EXPECT_EQ((size_t)3, t1.size());
        EXPECT_EQ(3, t1.back())          << "Nem jo erteket szunettet meg"  << std::endl;
        t1.pop_back();
        EXPECT_EQ((size_t)2, t1.size());
        EXPECT_EQ(2, t1.back())          << "Nem jo erteket szunettet meg"  << std::endl;
        t1.pop_back();
        EXPECT_EQ((size_t)1, t1.size());
        EXPECT_EQ(1, t1.back())    << "Nem jo erteket szuntettet meg!"  << std::endl;
        t1.pop_back();
        EXPECT_EQ((size_t)0, t1.size());
        EXPECT_TRUE(t1.empty())              << "Nem ures?" << std::endl;
    } ENDM;
#endif

/// Itt a vége
    if (ELKESZULT <= 0)
      FAIL() << "\n\nDefinialja az ELKESZULT makrot a tar.hhp-ben!" << std::endl;
    std::cout << "ELKESZULT = " << ELKESZULT << std::endl;
    if (ELKESZULT > 0 && !gtest_lite::test.fail()) {
      int kesz[] = { 1, 3, 5, 7, 9, 10, 12, 13, 14, 15 };
      int i = 9;
      while (i >= 0 && kesz[i] > ELKESZULT) --i;
      std::cout << "IMSC pont: " << i+1 << std::endl;
    }
    GTEND(std::cerr);       // Csak C(J)PORTA működéséhez kell
    return 0;
}
