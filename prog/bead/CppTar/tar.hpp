#include <type_traits>
#include <vector>
#include <stdexcept>
#include "memtrace.h"

#define ELKESZULT 15

template<typename T>
struct delete_if_ptr {
    void operator()(T t) {}
};

template<typename T>
struct delete_if_ptr<T*> {
    void operator()(T* t) {
        delete t;
    }
};

template<typename T, class C = std::vector<T> >
class Tar: public C {
public:
    template<typename iter>
    Tar(iter begin, iter end): C(begin, end) {}
    Tar(size_t s, T t): C(s, t) {}
    Tar(size_t s): C(s) {}
    Tar(): C() {}
    ~Tar() {
        this->clear();
    }
    
    Tar(const Tar& t): C(t) {
        if (std::is_pointer<T>::value)
            throw std::domain_error("");
    }
    
    Tar& operator=(const Tar& t) {
        if (std::is_pointer<T>::value)
            throw std::domain_error("");
        if (this == &t)
            return *this;
        C::operator=(t);
        return *this;
    }
    
    typedef typename C::iterator iterator;
    
    void clear() {
        for (T& elem: *this)
            delete_if_ptr<T>()(elem);
        C::clear();
    }
    
    iterator erase(iterator it) {
        delete_if_ptr<T>()(*it);
        return C::erase(it);
    }
    
    iterator erase(iterator begin, iterator end) {
        for (iterator i = begin; i != end; i++)
            delete_if_ptr<T>()(*i);
        return C::erase(begin, end);
    }
    
    void pop_back() {
        delete_if_ptr<T>()(C::back());
        C::pop_back();
    }
    
    template<typename func>
    void traverse(func f) {
        for (T& elem: *this)
            f(elem);
    }
};