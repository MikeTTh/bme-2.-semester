/**
 *  \file: bacterium.h
 *
 *  TestClass alaposztály és leszármazottjai
 */

#ifndef TESTCLASS_H
#define TESTCLASS_H

#include <string>

/// TestClass osztály.
class TestClass {
    static int numobj;
public:
    TestClass() { numobj++; }
    virtual void print(std::ostream& os) const {}
    static int get() { return numobj; }
    virtual ~TestClass() { numobj--; }
};

int TestClass::numobj = 0;

/// TestClass1 osztály.
class TestClass1 : public TestClass {
    int adat;
    int id;
public:
    TestClass1(int a = 0) : adat(a), id(get()) { }
    int getId() const { return id; }
    void print(std::ostream& os) const {
        os << adat << ';';
    }
};

/// TestClass2 osztály.
class TestClass2 : public TestClass {
    std::string str;
public:
    TestClass2(std::string s = "") : str(s) {}
    void print(std::ostream& os) const {
        os << str << ';';
    }
};

/// Funktor a kiíráshoz
/// Konstruktor paraméterben megadott adatfolyamra ír
class TestClass2Print {
    std::ostream& os;
public:
    TestClass2Print(std::ostream& os = std::cout) :os(os) {}
    void operator()(TestClass2 p) {
        p.print(os);
    }
};

/// Funktor a kiíráshoz
/// Konstruktor paraméterben megadott adatfolyamra ír
class TestClassPrint {
    std::ostream& os;
public:
    TestClassPrint(std::ostream& os = std::cout) :os(os) {}
    void operator()(TestClass *p) {
        p->print(os);
    }
};

#endif // TESTCLASS_H
