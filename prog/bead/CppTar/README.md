**IMSC feladat**

A feladat a nagy zárthelyi 2. feladatának egy módosított változata.

**Készítsen** adapter sablont (Tar), ami minden szabványos sorozattárolóra alkalmazható és segíti a 
pointerekkel hivatkozott objektumok tárolását, ugyanakkor **képes nem pointer** típusú adatokat is tárolni. 
Az adapter a sorozattárolók működését/tulajdonságait csupán a következőkben változtatja meg:

1. Nem csak a tárolóba tett adatokat birtokolja, hanem amennyiben az adat pointer, akkor a 
hivatkozott objektumokat is, így azokat megszünteti, ha a tároló megszűnik, vagy elemeit 
törlik (clear(), erase(), pop_back()). (Feltételezzük, hogy a pointer a dinamikus területre mutat.) 
2. Van egy traverse() tagfüggvénye is, ami a tároló minden adatával meghívja a paraméterként kapott 
egyoperandusú függvényt, vagy funktort.

A többi metódust, típust változatlanul elérhetővé teszi még akkor is, ha azok használatával a hivatkozott 
objektumok "felügyelete" kikerülhető. Az adapter sablonparaméterként vegye át a tárolandó pointertípust és a tárolót. 
Amennyiben ez utóbbi nincs megadva, akkor azt az *std::vector* sablonból hozza létre! 
Az alábbi kódrészlet a sablon példányosítására és használatára mutat példát:

    Tar<Ember\*> t1; 
    Tar<Ember\*, std::deque<Ember\*> > t2; 
    t1.push_back(new Ember); // Létrehoztunk Ember objektumot, és "rábíztuk" a tárolóra.
    t1.push_back(new Ember);
    t1.traverse(func1);      // t1-ben tárolt minden adattal (pointerrel) meghívjuk a func1 egyoperandusú függvényt, vagy funktort.
    Tar<Ember> t3;
    t3.push_back(Ember());	
    t3.traverse(func3);      // t3-ben tárolt minden adattal meghívjuk a func3 egyoperandusú függvényt, vagy funktort.
Ahol:

- t1 - *Ember\** típusú pointereket tárol *std::vector* segítségével.
- t2 - *Ember\** típusú pointereket tárol *std::deque* segítségével.
- t3 - *Ember* objektumokat tárol *std::vector* segítségével. 
   
Amennyiben a tárolóban pointer van, úgy a *Tar* osztályból létrehozott példányok *másolásakor*, 
vagy *értékadásakor* **dobjon** *std::domain:error* kivételt!


**Feladatok:**

1. Töltse le az előkészített projektet a tantárgy git tárolójából és elezze a kódot! 
   [https://git.ik.bme.hu/Prog2/ell_feladat/CppTar](https://git.ik.bme.hu/Prog2/ell_feladat/CppTar)
2. Készítse el a **Tar** osztályt a *tar.hpp* állományban, amiben definiálja az **ELKESZULT** makrót 
a sablon készültségi fokának megfelelő értékkel (1-15)!
4. Tesztelje a megadott tesztprogrammal a sablont! 
5. Amennyiben a tesztek jól futnak, töltse fel a *JPorta* feladatbeadó rendszerbe a **Tar.hpp** fájlt!

**Megjegyzés:**

- Nem kell minden részfeladatot elkészítenie. Az **ELKESZULT** makró értékét arra a legmagasabb értékre állítsa, amivel még 
hibamentesen fut a teszt.
- **IMSC** pontot az **ELKESZULT** makró értekétől függően kap! A kódban megtalalálja, hogy milyen értékhez hány pont tartozik.