/**
 * \file: hetero_store.hpp
 *
 */

#ifndef HETEROSTORE_HPP
#define HETEROSTORE_HPP

#include <iostream>
#include <stdexcept>

template<typename T, size_t c = 100, class e = std::out_of_range>
class HeteroStore {
    T* array[c];
    size_t s;
    HeteroStore& operator=(const HeteroStore&){}
    HeteroStore(const HeteroStore&) {}
public:
    HeteroStore(): s(0) {}
    ~HeteroStore() {
        clear();
    }
    size_t size() {
        return s;
    }
    size_t capacity() {
        return c;
    }
    void add(T* p) {
        if (s >= c) {
            delete p;
            throw e("");
        }
        array[s++] = p;
    }
    
    template<typename func>
    void traverse(func f) {
        for (size_t i = 0; i < s; ++i) {
            f(array[i]);
        }
    }
    void clear() {
        for (size_t i = 0; i < s; ++i) {
            delete array[i];
        }
        s = 0;
    }
};

#endif // HETEROSTORE_HPP
