﻿/**
 * \file: clonable_hetero_store.hpp
 *
 */

#ifndef CLONABLE_HETERO_STORE_HPP
#define CLONABLE_HETERO_STORE_HPP

#include <algorithm>
#include <vector>
#include <stdexcept>
#include "memtrace.h"

template<typename T, typename Store = std::vector<T*>, class except = std::out_of_range>
class ClonableHeteroStore: public Store {
public:
    void add(T* p) {
        try {
            this->push_back(p);
        } catch (std::bad_alloc& e) {
            delete p;
            throw except("");
        }
    }
    void clear(){
        for (typename Store::const_iterator i = this->begin(); i != this->end(); i++) {
            delete *i;
        }
        Store::clear();
    }
    ClonableHeteroStore(const ClonableHeteroStore& c) {
        for (typename Store::const_iterator i = c.begin(); i != c.end(); i++) {
            add((*i)->clone());
        }
    }
    ClonableHeteroStore& operator=(const ClonableHeteroStore& c) {
        if (this == &c) {
            return *this;
        }
        this->clear();
        for (typename Store::const_iterator i = c.begin(); i != c.end(); i++) {
            add((*i)->clone());
        }
        return *this;
    }
    ClonableHeteroStore() {}
    ~ClonableHeteroStore() {
        this->clear();
    }
};

#endif // CLONABLE_HETERO_STORE_HPP
