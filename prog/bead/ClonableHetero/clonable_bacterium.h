/**
 * \file: clonable_bacterium.h
 *
 */

#ifndef CLONABLE_BACTERIUM_H
#define CLONABLE_BACTERIUM_H

#include "bacterium.h"
#include "clonable.h"
#include "memtrace.h"


/**
 * A ClonableBacterium, ClonableSalmonella, ClonableStreptococcus
 * osztályoknak kompatibilisnek kell lennie a Bacterium és Clonable osztályokkal.
 * A funkciókat tekintve meg kell egyezni a Bacterium, Salmonella, Streptococcus
 * osztályok funkcióival.
 */

class ClonableBacterium: public virtual Bacterium, public Clonable {
public:
    ClonableBacterium(const char* n = ""): Bacterium(n) {}
    ClonableBacterium* clone() const {
        return new ClonableBacterium(*this);
    }
};

class ClonableSalmonella: public Salmonella, public ClonableBacterium {
public:
    ClonableSalmonella(const char* n = ""): Bacterium("Salmonella"), Salmonella(n) {}
    ClonableSalmonella* clone() const {
        return new ClonableSalmonella(*this);
    }
};

class ClonableStreptococcus: public Streptococcus, public ClonableBacterium {
public:
    ClonableStreptococcus(const char n): Bacterium("Streptococcus"), Streptococcus(n) {}
    ClonableStreptococcus* clone() const {
        return new ClonableStreptococcus(*this);
    }
};

#endif // CLONABLE_BACTERIUM_H
