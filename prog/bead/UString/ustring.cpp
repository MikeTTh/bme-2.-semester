#include "ustring.h"
#include <cstring>

bool UString::ucase = false;

std::ostream& operator<<(std::ostream& o, UString& u){
    if (u.UCase()) {
        for (size_t i = 0; i < u.size(); i++) {
            o << (char)toupper(u[i]);
        }
        return o;
    } else {
        return o << (String)u;
    }
}
