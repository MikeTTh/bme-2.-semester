#include "string5.h"
#include <iostream>

#ifndef USTRING_H
#define USTRING_H

class UString: public String {
    static bool ucase;
public:
    UString(const char* s = ""): String(s) {}
    UString(const char c): String(c) {}
    UString(const String& s): String(s) {}
    UString(const UString& u): String(u) {}
    
    static void UCase(bool b) {
        ucase = b;
    }
    static bool UCase() {
        return ucase;
    }
};

std::ostream& operator<<(std::ostream& o, UString& u);
#endif // USTRING_H
