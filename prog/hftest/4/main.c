#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

char myid[]   = "To'th Miklo's Tibor,1MI,IMSC2,L3-IMSC <tothmiklostibor@gmail.com> FLGIIG\n";
char mytask[] = "Feladat = 4 ISO 8859-2\n";

void joSor() {
    printf("a1V6a");
    int r = rand() % 40;
    for (int i = 0; i < r; ++i) {
        printf("%c", (rand()%26)+65);
    }
    printf("AammeseA\n");
}

int main() {
    srand(time(0));
    setbuf(stdout, NULL);        // Kikapcsolja a bufferelest
    printf("%s", myid);
    printf("%s", mytask);
    for (int i = 0; i < 7; ++i) {
        joSor();
    }
    for (int i = 0; i < 220-7; ++i) {
        printf("######\n");
    }
    
    return 0;
}