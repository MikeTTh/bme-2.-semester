#include "f10.h"
#include "cstring"
#include "memtrace.h"

const char * F10::ident() const {
    return "To'th Miklo's Tibor,1MI,IMSC2,L3-IMSC <tothmiklostibor@gmail.com> FLGIIG\n";
}

bool F10::match(const char* input) {
    if (input == NULL)
        return false;
    
    bool found = false;
    
    if (strstr(input, "alapjai") != NULL)
        found = true;
    if (strstr(input, "dollarjelbol") != NULL)
        found = true;
    if (strstr(input, "utasitasonkenti") != NULL)
        found = true;
    
    if (found) {
        if (line != NULL)
            delete[] line;
        
        len = strlen(input);
        line = new char[len+1];
        strcpy(line, input);
    }
    
    return found;
}

const char* F10::getLine() const {
    return line;
}

F10::F10() {
    len = 0;
    line = NULL;
}

F10::F10(const F10& f10) {
    if (f10.getLine() == NULL){
        len = 0;
        line = NULL;
        return;
    }
    len = strlen(f10.getLine());
    line = new char[len + 1];
    strcpy(line, f10.getLine());
}

F10::~F10() {
    delete[] line;
}

F10& F10::operator=(const F10& f10) {
    if (this == &f10) {
        return *this;
    }
    
    delete[] line;
    
    if (f10.getLine() == NULL) {
        len = 0;
        line = NULL;
        return *this;
    }
    
    len = strlen(f10.getLine());
    line = new char[len + 1];
    strcpy(line, f10.getLine());
    
    return *this;
}