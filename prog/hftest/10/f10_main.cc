/**
 * Main program for testing your F10 class
 * for To'th Miklo's Tibor, FLGIIG
 * Date: 2020-04-20 19:45:54
 * This file can be freely modified
 */

#include <iostream>
#include "f10.h"

using std::cout;
using std::endl;
using std::cin;

F10 fv(F10 f, int n) {
  F10 a = f;
  if (n)
    a = fv(f, n-1);
  return a;
}

int main() {
  F10 a, c;

  cout << a.ident() << endl;
  cout << a.match("xxa asalapjai as \n") << endl;
  for (int i = 0; i < 5; i ++) { 
    F10 b = a;
    cout << b.match(" dollarjelbol ") << endl;
  }
  F10 b = a;
  a = a;
  c = b;

  // kicsit nyuzzuk...

  c = fv(c, 100);

  cout << c.getLine();
  return 0;
}
