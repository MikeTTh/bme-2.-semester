#
# makefile az F10 osztaly kiprobalasahoz.
# To'th Miklo's Tibor, FLGIIG reszere
# Datum: 2020-04-20 19:45:54
# Ez az allomany tetszes szerint modosithato, de nem celszeru
#

CC = g++
CXXFLAGS = -Wall -Werror -pedantic -DMEMTRACE
HFT = /home/a/tutors/szebi/hftest2
HEADS = f10.h

f10: f10_main.o f10.o memtrace.o
	$(CC) -Wall -o f10 f10_main.o f10.o memtrace.o

f10.o: $(HEADS)

f10_main.o: $(HEADS)

memtrace.o:	memtrace.h

submit: f10
	$(HFT) -10

clean: 
	rm -f f10 f10_main.o f10.o memtrace.o

