#
# makefile az F8 feladat kiprobalasahoz.
# To'th Miklo's Tibor, FLGIIG reszere
# Datum: 2020-04-11 16:58:10
# Ez az allomany tetszes szerint modosithato, de nem celszeru
#

CXX = g++
CXXFLAGS = -Wall -pedantic
HFT = /home/a/tutors/szebi/hftest2
HEADS = f8.hpp

f8: f8_main.o 
	$(CXX) -Wall -pedantic -o f8 f8_main.o

f8_main.o: $(HEADS)

submit: f8
	$(HFT) -8


