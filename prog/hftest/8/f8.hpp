#ifndef F8_HPP
#define F8_HPP

#include <algorithm>
#include <iostream>
#include <cstddef>
#include <vector>
#include <iomanip>
#include <stdexcept>

template<size_t I>
class F8 {
    double tomb[I];
    size_t size;
public:
    F8(): size(0) {}
    double& operator[](size_t i){
        if (i == size)
            size++;
        else if (i > size || i > I || i < 0)
            throw std::out_of_range("Túlindexelés");
        return tomb[i];
    }
    double operator[](size_t i) const {
        if (i >= size || i >= I || i < 0)
            throw std::out_of_range("Túlindexelés");
        return tomb[i];
    }
    const char* c_str(){
        return "To'th Miklo's Tibor,1MI,IMSC2,L3-IMSC <tothmiklostibor@gmail.com> FLGIIG";
    }
    void rendez() {
        std::sort(tomb, tomb + size);
    }
};

template<size_t I>
std::ostream& operator<<(std::ostream& os, F8<I>& f8) {
    return os << std::fixed << std::setprecision(4) << f8[24] << std::endl
        << f8[38] << std::endl
        << f8[101] << std::endl
        << f8[100] << std::endl
        << f8[434] << std::endl;
}

#endif