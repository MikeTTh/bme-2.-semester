#include <stdio.h>
#include <stdlib.h>
#include <time.h>

char myid[]   = "To'th Miklo's Tibor,1MI,IMSC2,L3-IMSC <tothmiklostibor@gmail.com> FLGIIG\n";
char mytask[] = "Feladat = 6 ISO 8859-2\n";

int joSor(char* sor) {
    if (*sor == '\0')
        return 0;
    
    if (*sor == '#')
        return 0;
    sor++;
    
    if (*sor == '\0')
        return 0;
    
    while (*sor == '_')
        sor++;
    
    if (*sor == '\0')
        return 0;
    
    int volt0 = 0;
    while (*sor == '0' || *sor == 'x'){
        if (*sor == '0')
            volt0 = 1;
        else
            volt0 = 0;
        sor++;
    }
    
    if (*sor == '\0')
        return 0;
    
    if (!((*sor >= '0' && *sor <= '9')||volt0))
        return 0;
    
    if (*sor == '\0')
        return 0;
    
    while (*sor >= '0' && *sor <= '9')
        sor++;
    
    if (*sor == '\0')
        return 0;
    
    if (*sor != 'V')
        return 0;
    sor++;
    
    if (*sor == '\0')
        return 0;
    
    if (*sor != '6')
        return 0;
    sor++;
    
    if (*sor == '\0')
        return 0;
    
    while (!(*sor >= 'A' && *sor <= 'Z')) {
        if (*sor == '\0')
            return 0;
        sor++;
    }
    
    if (*sor == '\0')
        return 0;
    
    while (*sor >= 'A' && *sor <= 'Z')
        sor++;
    
    while (*sor != '\0')
        sor++;
    
    sor-=6;
    
    if(*sor != 'm')
        return 0;
    sor++;
    
    if (*sor == '\0')
        return 0;
    
    if(*sor != 'm')
        return 0;
    sor++;
    
    if (*sor == '\0')
        return 0;
    
    if(*sor != 'e')
        return 0;
    sor++;
    
    if (*sor == '\0')
        return 0;
    
    if(*sor != 's')
        return 0;
    sor++;
    
    if (*sor == '\0')
        return 0;
    
    if(*sor != 'e')
        return 0;
    sor++;
    
    if (*sor == '\0')
        return 0;
    
    if(!(*sor == 'A' || *sor == 'a'))
        return 0;
    sor++;
    
    if (*sor == '\0')
        return 1;
    
    return 0;
}

int main() {
    srand(time(0));
    setbuf(stdout, NULL);        // Kikapcsolja a bufferelest
    printf("%s", myid);
    printf("%s", mytask);
    
    char sor[1000000];
    
    int sorok = 0, illeszkedik = 0;
    
    
    while (gets(sor) != NULL){
        fprintf(stderr, "%s\n", sor);
        sorok++;
        if (joSor(sor))
            illeszkedik++;
    }
    
    printf("%o/%o\n", sorok, illeszkedik);
    fprintf(stderr, "%o/%o\n", sorok, illeszkedik);
    
    return 0;
}