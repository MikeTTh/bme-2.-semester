#include "f12.h"
#include <cstring>

double mPow(double x, int n) {
    double r = 1;
    for (int i = 0; i < n; ++i) {
        r *= x;
    }
    return r;
}

Queue::Queue(const char *p) {
    if (p == NULL) {
        return;
    }
    for (size_t i = 0; i < strlen(p); ++i) {
        q.push_back(p[i]);
    }
}

char Queue::pop_back() {
    if (q.empty()) {
        throw std::underflow_error("Empty");
    }
    char tmp = q.back();
    q.pop_back();
    return tmp;
}

void Queue::push_front(const char c) {
    q.push_front(c);
}

char Queue::pop_front() {
    if (q.empty()) {
        throw std::underflow_error("Empty");
    }
    char tmp = q.front();
    q.pop_front();
    return tmp;
}

bool Queue::empty() {
    return q.empty();
}

Queue::myIterator Queue::begin() {
    return q.begin();
}

Queue::myIterator Queue::end() {
    return q.end();
}

const char* F2::myId = "To'th Miklo's Tibor,1MI,IMSC2,L3-IMSC <tothmiklostibor@gmail.com> FLGIIG";

double F2::f(double x) const {
    if (x > 50) {
        return x/108.65;
    } else {
        return 0.424*mPow(x, 4) - 0.848*mPow(x, 3) + 54.25*mPow(x, 2) + 2*x - 50;
    }
}
