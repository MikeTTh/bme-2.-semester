/**
 * foprogram az F12 osztaly tesztelesehez
 * To'th Miklo's Tibor, FLGIIG reszere
 * Datum: 2020-05-12 00:17:52
 * Ez az allomany tetszes szerint modosithato
 */

#include <iostream>
#include "f12.h"

using namespace std;

/*
 * F2 interfeszt varunk
 */ 
void solveF2(const F2& f2) {
  cout << f2.myId << endl;
  cout << "Feladat = 2" << endl;

  double d;
  while (cin >> d) 
    cout << f2.f(d) << endl;
}

/*
 * Queue interfeszt varunk
 */ 
void printQ(Queue& t) {
  for (Queue::myIterator i1 = t.begin(); i1 != t.end(); ) 
     std::cout << *i1++;
}

/*
 * Segedfuggvenyek a tesztekhez
 */
void reverseQ(Queue& q1, Queue& q2) {
  for (Queue::myIterator i1 = q1.begin(); i1 != q1.end(); i1++) {
     q2.push_front(*i1);
  }
}

bool operator==(Queue& q1, Queue& q2) {
  if (q1.size() != q2.size())
    return false;
  Queue::myIterator i1 = q1.begin();
  for (Queue::myIterator i2 = q2.begin(); i2 != q2.end(); i2++, i1++) {
    if (*i1 != *i2)
    return false;
  }
  return true;
}

bool operator!=(Queue& q1, Queue& q2) {
  return !(q1 == q2);
}

/*
 * Foprogram
 */
int main() {
  F12 s0;
  if (s0.size() != 0) {
    cout << "s0 Miert nem ures?" << endl;
	return 1;
  }

  F12 s1("it sutsz kis szucs");
  F12 s2 = s1;
  if (s1 != s2) {
    cout << "Mi torten a masoloval?" << endl;
    return 1;
  }

  s0 = s2;
  if (s0 != s2) {
    cout << "Mi torten az op=-vel?" << endl;
    return 1;
  }

  F12 sr;
  reverseQ(s0, sr);

  while (!s0.empty()) s0.pop_front();

  if (s0.size() != 0) {
    cout << "Miert nem ures? Most toroltuk!" << endl;
	return 1;
  }
  
  reverseQ(sr, s0);
  if (s0 != s2) {
    cout << "Mi torten?" << endl;
    return 1;
  }
 
  try {
    s2.push_back('?');
    s2.push_back('\n');
    s2.push_front('M');

    cout << "2. feladat tesztelese. double olvasas EOF-ig:" << endl;
    solveF2(s1);

    printQ(s2);

    while (1) s1.pop_front();
    
  } catch (underflow_error e) {
    cout << "undeflow_error: " << e.what() << endl;
  }

  return 0;
}
