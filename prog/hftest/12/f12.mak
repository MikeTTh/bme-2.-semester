#
# Makefile az F12 osztaly tesztelesehez
# To'th Miklo's Tibor, FLGIIG reszere
# Datum: 2020-05-12 00:17:52
# Ez az allomany tetszes szerint modosithato, de nem celszeru
#

CC = g++
CXXFLAGS = -Wall -Werror -pedantic
HFT = /home/a/tutors/szebi/hftest2
HEADS = f12.h

f12: f12_main.o f12.o
	$(CC) -o f12 f12_main.o f12.o

f12.o: $(HEADS)

f12_main.o: $(HEADS)


submit: f12
	$(HFT) -12


