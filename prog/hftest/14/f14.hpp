#ifndef F14_HPP
#define F14_HPP

#include <vector>
#include <cstdlib>
#include <stdexcept>
#include <iostream>

template<typename T>
class F14: public std::vector<T> {
public:
    F14(){}
    
    template<typename a>
    F14(a A): std::vector<T>(A) {}
    
    template<typename a, typename b>
    F14(a A, b B): std::vector<T>(A, B) {}
    
    T get_back() {
        if (this->empty()) {
            throw std::out_of_range("Empty");
        }
        
        T val = this->back();
        this->pop_back();
        
        return val;
    }
};

template<>
int F14<int>::get_back() {
    if (this->empty()) {
        throw std::out_of_range("Empty");
    }
    
    int val = this->back() - 50;
    this->pop_back();
    
    return val;
}

template<>
std::string F14<std::string>::get_back() {
    return "To'th Miklo's Tibor,1MI,IMSC2,L3-IMSC <tothmiklostibor@gmail.com> FLGIIG";
}

template <typename T>
class osFunctor {
private:
    std::ostream& out;
    std::string delimiter;

public:
    explicit osFunctor(std::ostream &os = std::cout, const std::string& delimiter = "")
            : out(os), delimiter(delimiter) {}
    
    void operator()(T element) {
        out << element << delimiter;
    }
};

#endif