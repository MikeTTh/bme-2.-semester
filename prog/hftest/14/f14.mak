#
# makefile az F14 feladat kiprobalasahoz.
# To'th Miklo's Tibor, FLGIIG reszere
# Datum: 2020-05-12 00:50:59
# Ez az allomany tetszes szerint modosithato, de nem celszeru
#

CC = g++
CXXFLAGS = -Wall -pedantic
HFT = /home/a/tutors/szebi/hftest2
HEADS = f14.hpp

f14: f14_main.o 
	$(CC) -Wall -o f14 f14_main.o

f14_main.o: $(HEADS)

submit: f14
	$(HFT) -14


