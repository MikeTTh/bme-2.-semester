#include <stdio.h>
#include <math.h>

double fx(double x) {
    if (x > 50.0){
        return x/108.65;
    }else{
        return 0.424 * pow(x, 4.0) - 0.848 * pow(x, 3.0) + 54.25 * pow(x, 2.0) + 2.0 * x - 50.0;
    }
}

char myid[]   = "To'th Miklo's Tibor,1MI,IMSC2,L3-IMSC <tothmiklostibor@gmail.com> FLGIIG\n";
char mytask[] = "Feladat = 2 ISO 8859-2\n";

int main() {
    setbuf(stdout, NULL);        // Kikapcsolja a bufferelest
    printf("%s", myid);
    printf("%s", mytask);
    
    double szam;
    while(scanf("%lf", &szam) != EOF) {
        printf("%lf\n", fx(szam));
    }
    return 0;
}