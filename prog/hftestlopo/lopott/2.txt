
Hello tm1743!
Tesztelem a "leker" programot...
Sikeres azonosítás:
   Tóth Miklós Tibor,1MI,IMSC2,L3-IMSC  <tothmiklostibor@gmail.com> FLGIIG
   3. feladat

Korábbi eredményei:
   1: 8/8   11: 2/2   13: 2/2   3: 4/4
   5: 4/4   7: 2/2   9: 2/2


4. feladat Tóth Miklós Tibor (FLGIIG) részére:
Írjon C programot, vagy shell scriptet, amely a standard outputra 
kiír egy olyan 220 sor hosszú szöveget, amelyben pontosan 7 olyan 
sor van, amelyre az alábbi reguláris kifejezés illeszkedik! Az illeszkedõ 
sorok legyenek egymástól különbözõek!

  ^[^#]_*(0x)*[0-9]+V6.+[A-Z]+.+mmese(A|a) 



A teszt eredménye: SIKERES (exit status:0,0) 3: 5/5
Összes hftest pontja: 0
