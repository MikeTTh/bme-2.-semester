#include <stdio.h>
#include <stdlib.h>

char template[] = "#include <stdio.h>\n"
                  "\n"
                  "char myid[]   = \"To'th Miklo's Tibor,1MI,IMSC2,L3-IMSC <tothmiklostibor@gmail.com> FLGIIG\\n\";\n"
                  "char mytask[] = \"Feladat = %d ISO 8859-2\\n\";\n"
                  "\n"
                  "int main() {\n"
                  "    setbuf(stdout, NULL);        // Kikapcsolja a bufferelest\n"
                  "    printf(myid);\n"
                  "    printf(mytask);\n"
                  "    return 0;\n"
                  "}";

int main(){
    FILE* f;
    for(int i = 1; i < 40; i +=2) {
        printf("%d. feladat\n", i/2+1);
        system("sleep 1");
        f = fopen("leker.c", "w");
        fprintf(f, template, i);
        fclose(f);
        f = NULL;
        system("sleep 1");
        system("gcc leker.c -o leker -std=c99");
        char* cmd = malloc(50*sizeof(char));
        sprintf(cmd, "bash -c \"hftest leker > ./out/%i.txt\"", i/2+1);
        system(cmd);
        system("sleep 1");
    }
    
    return 0;
}