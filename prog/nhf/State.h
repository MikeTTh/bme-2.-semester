//
// Created by mike on 24/04/2020.
//

#ifndef NHF_STATE_H
#define NHF_STATE_H


#include "View.h"
#include "Window.h"
#include "Game.hpp"
#include "SDL.h"

#include "memtrace.h"

/// @brief State absztrakt osztály. A játék egy adott állapota.
class State: public View {
protected:
    /// @brief A következő állapot változója, amennyiben maradni szeretnénk a jelenlegi állapotban, akkor az objektumra mutató pointer
    State* next;
public:
    /**
     * @brief Alap konstruktor
     * @param n - next értéke
     */
    State(State* n = NULL): next(n) {}
    virtual void draw(Window&, Game&) = 0;
    virtual void interact(SDL_Event*, Window&, Game&) = 0;
    /// @brief A következő állapotra mutató pointert ad vissza, a felszabadítást nem végzi el.
    State* getNext() {
        return next;
    }
    virtual ~State(){}
};

#endif //NHF_STATE_H
