#ifndef NHF_BUTTON_HPP
#define NHF_BUTTON_HPP
#include "View.h"
#include "Position.h"
#include "TextBox.h"
#include <string>
#include "SDL.h"
#include "State.h"

#include "memtrace.h"

/// @brief kiválasztott gomb színe
const SDL_Color selected = {0, 255, 0, 255};
/// @brief nem kiválasztott gomb színe
const SDL_Color unselected = {255, 255, 255, 255};

/**
 * @brief Megadja, hogy egy koordináta benne van-e a téglalapban.
 * @param rect - a megadott téglalap
 * @param x
 * @param y
 * @return Ha benne van, true; ha nincs, false.
 */
inline bool inRect(SDL_Rect* rect, int x, int y){
    return y > rect->y && y < rect->y + rect->h && x > rect->x && x < rect->x + rect->w;
}

/// @brief ButtonAbstr absztrakt osztály. Arra van, hogy lehessen több féle függvényű Button-höz tárolót csinálni.
class ButtonAbstr: public View {
public:
    virtual void draw(Window&, Game&) = 0;
    void interact(SDL_Event*, Window&, Game&) = 0;
    
    /// @brief A gombot áthelyezi.
    virtual void setPos(SDL_Rect& r) = 0;
    
    /// @brief Beállítja, hogy a gomb melyik bool alapján színezze magát.
    virtual void setSel(const bool *b) = 0;
    
    virtual ~ButtonAbstr(){}
};

/**
 * @brief Kattintható gombok.
 * @tparam func - A kattintásra lefutó függvény típúsa. Lehet függvénypointer vagy functor.
 */
template<class func>
class Button: public ButtonAbstr {
    /// @brief A gomb pozíciója.
    SDL_Rect pos;
    /// @brief A kattintásra lefutó függvény.
    func onClick;
    /// @brief Pointer egy bool értékre, amely alapján színez a gomb.
    const bool* select;
    /// @brief Megadja, hogy invertáljuk-e a select-et invertáljuk-e.
    bool invert;
    /// @brief A TextBox, amely valójában kirajzolja a szöveget.
    TextBox tb;
public:
    explicit Button(func f, SDL_Rect p, const std::string& n, bool invert = false, const bool* sel = NULL): pos(p), onClick(f), select(sel), invert(invert), tb(TextBox(n, (SDL_Color){255, 255, 255, 255}, p)) {}
    void draw(Window& w, Game& g) {
        if ((select == NULL || *select) ^ invert) {
            SDL_SetRenderDrawColor(w.getRenderer(), selected.r, selected.g, selected.b, selected.a);
        } else {
            SDL_SetRenderDrawColor(w.getRenderer(), unselected.r, unselected.g, unselected.b, unselected.a);
        }
        SDL_RenderDrawRect(w.getRenderer(), &pos);
        tb.draw(w, g);
    }
    void interact(SDL_Event* ev, Window& w, Game& g) {
        Position p(ev->button.x, ev->button.y);
    
        if (ev->type == SDL_MOUSEBUTTONDOWN && inRect(&pos, ev->button.x, ev->button.y)) {
            onClick();
        }
    }
    void setPos(SDL_Rect& r) {
        pos = r;
        tb.setPos(r);
    }
    void setSel(const bool *b) {
        select = b;
    }
};


#endif //NHF_BUTTON_HPP