//
// Created by mike on 24/04/2020.
//

#ifndef NHF_POSITION_H
#define NHF_POSITION_H

#include "SDL.h"

#include "memtrace.h"

/// @brief A pályán egy egység méretű négyzetet ír le.
class Position {
public:
    double x;
    double y;
    explicit Position(double x = 0, double y = 0);
    
    /// @brief Két egység méretű pont teljes egybeesését mutatja
    bool operator==(const Position& p) const;
    
    /// @brief Két egység méretű pont közti ütközést ellenőrzi
    bool operator<=(const Position& p) const;
    
    /// @brief Két egység méretű pont közti ütközést ellenőrzi
    bool operator>=(const Position& p) const;
    /**
     * @brief A pálya körkörösségéért felelő függvény.
     * Ellenőrzi, hogy a pályán kívül van-e a pont, és ha igen, akkor átteszi a pálya másik szélére.
     * @param x - pálya szélessége
     * @param y - pálya magassága
     */
    void loopAround(unsigned int x, unsigned int y);
    /**
     * @brief Egy véletlenszerű pozíciót generál a pályán belül.
     * @param x - pálya szélessége
     * @param y - pálya magassága
     */
    void randomize(unsigned int x, unsigned int y);
};



#endif //NHF_POSITION_H
