//
// Created by mike on 27/04/2020.
//

#include "FoodView.h"

#include "memtrace.h"

void FoodView::draw(Window& w, Game& g) {
    SDL_Rect block;
    block.x = (int)(w.getUnit()*g.food.x);
    block.y = (int)(w.getUnit()*g.food.y);
    block.h = block.w = (int)w.getUnit();
    SDL_SetRenderDrawColor(w.getRenderer(), 255, 0, 0, 0);
    SDL_RenderFillRect(w.getRenderer(), &block);
}

void FoodView::interact(SDL_Event *, Window &, Game &) {}
