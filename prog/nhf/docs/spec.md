# Programozás alapjai 2 NHF specifikáció


## Feladat

A *Kígyó* feladatot választottam a nagy házi listából.  
Ennek a feladatkiírása:  
&nbsp; 

*__Kígyó__  
Tervezzen objektummodellt kígyójáték objektumainak leírására! Legyen pálya, kígyó, gyümölcs, amit a kígyó fel tud szedni. Határozza meg az objektumok kapcsolatát és felelősségét!  
Demonstrálja a működést külön modulként fordított tesztprogrammal! A játék állását nem kell grafikusan megjeleníteni, elegendő csak karakteresen, a legegyszerűbb formában! A megoldáshoz ne használjon STL tárolót!*
  
&nbsp;  

A *kígyójáték* alatt pedig egy olyan játékot értünk, amelyben egy kígyó a felhasználótól bekért irányba halad 
és egy 2 dimenziós pályán lévő ételhez érve 1 egység hosszal nő.

## Feladatspecifikáció

A feladatkiírás nem követeli meg a grafikus megjelenítést, azonban
én grafikusan szeretném megoldani, így játék az SDL könyvtár segítségével fog rajzolni a képernyőre.
A pályákat szöveges fájlokból lehet beolvasni, illetve a játékban lesz többjátékos mód is.

## A pálya

A pályát leíró szöveges fájlban egy X az olyan 1\*1 egységni területet (ahol a kígyó feje és további elemei is mind 1\*1 egység méretűek) jelent, amibe beleütközve meghal a kígyó,
az S az a kígyó kiindulási helyét jelöli, minden más karakter olyan helyet jelent, ahol lehet mozogni.
Példaul ez a pálya:
```
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

  S

            XXXX
            XXXX



XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
```
egy 30\*10 egység méretű pályát eredményezne, amelyben a kígyó a (3, 3) helyen kezd és a pálya alja és teteje közt nem lehet átmenni, illetve a közepén található egy 4\*2 egység méretű tiltott hely.

## Multiplayer

A játékban található sigle- és multiplayer mód is, vagyis egyedül vagy több játékossal is játszható.
Ezek a módok közül a főmenüben lehet választani.

## Cél

A játékban 2 féle játékmenet közül lehet választani:  

* normál mód
    * egy bizonyos pontszám elérésével lehet nyerni
    * aki előbb eléri, az nyer
* végtelen mód
    * a kígyó haláláig megy a játék
    * aki utolsónak életben marad, az nyer

## Tesztelés

A programom tesztelésénél a probléma komplexitása miatt nem valószínű, hogy a grafikus megjelenítés is tesztelve lesz.
A program nem szigorúan grafikus részei viszont tesztelve lesznek, előreláthatólag a `gtest_lite.h` könyvtárral.