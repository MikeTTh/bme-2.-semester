# Sznék programozói dokumentáció
A programozás alapjai I.   
Tóth Miklós - FLGIIG

## Programstruktúra

### `snake_common.h/.c`
Az olyan változókat, adatszerkezeteket és függvényeket tartalmazza,
amelyek sok helyen használva vannak a programban, de nem köthetőek
egy specifikus programrészhez.

#### Változók
```c
int resX;
int resY;
```
A képernyő felbontását tárolja.

```c
#define speed 15.0
```
A kígyó beállított haladási sebessége.

```c
#define sampletime 240.0
```
A kígyó (nem fej) elemeinek mozgásának mintavételezési sebessége.

#### Adatszerkezetek

```c
typedef enum gameState {
    win,
    game,
    menu,
    done,
} gameState;
```   
A játék jelenlegi állapotát tárolja. Megadja, hogy menüben vagy játékban vagyunk vagy nyertünk, illetve, hogy bezárás közben vagyunk.

```c
typedef struct gamemode {
    bool multiplayer;
    bool death;
} gamemode;
```
A játékmódot tárolja.

```c
typedef enum {
    open,
    block,
    spawn
} square;
```
A pálya egy elemének típusát adó enum.

```c
typedef struct {
    double x, y;
} position;
```
Egy `x, y` koordinátapárost tároló struct.

```c
typedef struct snakeNode {
    position direction;
    position pos;
    struct snakeNode* next;
    struct snakeNode* prev;
    SDL_Color color;
    int score;
    bool visible;
} snakeNode;
```
A kígyó egy "darabja". Ez egy kétszeresen láncolt lista egy eleme.
A `bool visible` pedig azt adja meg, hogy az adott elem az ki lesz rajzolva
vagy pedig csak a pozícionálás miatt számít.  
A választás azért került a kétszeresen láncolt listára,
mint fő adatstruktúra, mivel a kígyó egyes darabjait
nem szükséges közvetenül elérni tudni, azonban mind előrefele, mind visszafele
haladva el kell tudni érni az előtte/mögötte lévő elemeket, ami ezzel az adatstruktúrával
hatékony és egyszerű. Emellett a folyamatos újrafoglalás (dinamikus tömb esetén)
nagyban rontott volna a hatékonyságon 

```c
typedef struct gameStruct {
    SDL_Renderer* renderer;
    gamemode gm;
    gameState state;
    SDL_Event event;
    TTF_Font* font;
    unsigned int lastTick;
    double unit;
    palya* p;
    snakeNode* players[2];
    SDL_TimerID playerTimers[2];
    position food;
    int winner;
} gameStruct;
```
A játék minden fázisában lényeges információk összességét eltároló struct.

#### Fügvények

```c
gameStruct initStruct(SDL_Renderer* renderer, TTF_Font* lexie,
    double unit, palya* p);
```
Inicializál egy `gameStruct` típust.

```c
void palyaFelszab(palya* p);
```
Felszabadít egy pályát. (és a benne lévő dinamikus 2D tömböt)

```c
void snakeFelszab(snakeNode* s);
```
Felszabadít egy `snakeNode*`-ből álló listát.

```c
void felszabadit(gameStruct* data);
```
Felszabadítja a `gameStruct`-ban tárolt adatokat. (meghívja a `palyaFelszab` és `snakeFelszab` fügvényeket is)

### `rajzol.h/.c`
A rajzolás zömét végző függvények.

#### Függvények

```c
bool inRect(SDL_Rect* rect, position mouse);
```
Megadja, hogy a megadott pozició benne van-e a megadott téglalapban.

```c
SDL_Renderer* sdlinit();
```
Az SDL-t inicializálja, létrehozza a játékot megjelenítő ablakot és a játékot oda rajzoló `renderer`-t, amit feketére is fest.

```c
void renderText(SDL_Renderer* renderer, TTF_Font* font,
    char* string, SDL_Rect* dest, SDL_Color color);
```
Egy paraméterként kapott szöveget a megadott betűtípussal lerenderel, majd a kapott téglalapra középre igazítja, az arányokat megtartva kitöltve.

```c
void drawSnakes(gameStruct* data);
```
A kígyókat a képernyőre rajzoló program.

```c
void drawPoint(SDL_Renderer* render, TTF_Font* lexie,
    snakeNode* s, SDL_Rect* pos);
```
Kirajzolja a paraméterként kapott kígyó pontszámát a kapott helyre, a kapott betűtípussal, a kígyó színével.

```c
void drawMaxPoint(SDL_Renderer* render, TTF_Font* lexie,
    int maxScore, SDL_Color color, SDL_Rect* pos);
```
Kirajzolja a maximum pontszámot a kapott színnel.

```c
void drawFood(gameStruct* data);
```
Kirajzolja zöld színnel az ételt.

```c
void drawPalya(gameStruct* data);
```
Kirajzolja a pálya szélét és a benne lévő akadályokat a képernyőt arányosan kitöltve.

```c
void buttonHandler(SDL_Renderer* render, TTF_Font* font, SDL_Rect* place, SDL_Color color, char* text, position mouse, buttonFunc func, void* params);
```
Gombkezelő fv., amely kirajzol egy gombot a megadott szöveggel, betűtíussal, színnel, a megadott helyen
és kattintáskor lefuttatja a megadott fv.-t. 

```c
bool rajzKezd(SDL_Renderer* render, gamemode* gm,
    TTF_Font* lexie, position mouse);
```
Kirajzolja a főmenüt.

### `logic.h/.c`
A kígyó mozgatásáért és a különböző ütközésekért/számításokért felelős függvények.

#### Változók
```c
SDL_Color valid[] = {
    {0xFC, 0xBA, 0x03, 255},
    {0x32, 0xA8, 0x52, 255}
};
```
A kígyók színei.

#### Függvények

```c
position loopAround(position pos, position max)
```
Egy jelenlegi és maximális pozíciót megadva visszaadja a pozíciót a pálya szélein átmenéssel számolva.

```c
void moveSnakes(gameStruct* data);
```
A kígyók fejeit mozgató függvény.

```c
bool checkHit(position a, position b);
```
Ellenőrzi, hogy a két egy egység méretű négyzet érinti vagy metszi-e egymást, ez esetbe az értéke `true`,
ha elkerülik egymást, akkor `false`.

```c
bool checkDeath(snakeNode* snake1, snakeNode* snake2);
```
Ellenőrzi, hogy `snake1` feje beleütközött-e `snake2` bármely részébe. Ütközés esetén `true`.

```c
bool checkWallHit(snakeNode* snake, palya p);
```
Ellenőrzi, hogy a kapott kígyó ütközik-e a kapott pálya bármely elemével.

```c
position randPos(position max);
```
`(.c)` Visszaad egy a `max` értéket meg nem haladó `position`-t.

```c
void newFood(gameStruct* data);
```
Új random helyre teszi az ételt, úgy, hogy az a pálya elemeivel ne ütközzön.

```c
void checkFood(gameStruct* data);
```
Ellenőrzi, hogy valamelyik kígyó ütközött-e az étellel, ez esetben növeli a 
pontszámát és hosszát és új helyre teszi az ételt.

```c
palya* palyaTolt(char* filename);
```
A megadott fájlnevű pályát beolvassa dinamikus memóriaterületre, majd a területre mutató pointert visszaadja.

```c
Uint32 snakeMover(Uint32 interv, void* param);
```
Egy `SDL_Timer` típusú függvény. Ez a függvény mozgatja a mintavételezési sebességgel a kígyók fejen kívüli részeit.

```c
double resize(position screen, position size);
```
A pálya és a képernyő méretének ismeretében visszaadja, hogy az 1 egység az hány pixelnek felel meg.

```c
void createSnake(int no, gameStruct* p);
```
Létrehoz és inicializál egy `snakeNode` típust, amit elhelyez a pálya megfelelő helyére, vagy ha nincs megadott hely, akkor random helyre.

### `eventloops.h/.c`
A játék különböző fázisaihoz tartozó event loopok. Lényegében ezek a függvények reagálnak az inputra.

#### Függvények

```c
void initLoop(gameStruct* data);
```
A főmenü event loopja.

```c
void gameLoop(gameStruct* data);
```
A játék event loopja.

```c
void endLoop(gameStruct* data);
```
A játék vége ("Vesztettél!"/"Nyertél!"-t kirajzoló) event loop.

### `main.c`

#### Függvények

```c
#ifdef _WIN32
int WinMain();
#endif
```
Ennek a függvénynek a hiányában nem tudom cross-compile-olni Windows-ra a programot mingw-vel,
feltehetően szükséges a Windows-on való futáshoz.

```c
int main(int argc, char *argv[]);
```
A program belépési pontja, ez tartalmazza a különböző event loopokat körülölelő közös részeket.

## Tesztelés

A programot magam is teszteltem, illetve a kódot nem ismerő barátaimmal is teszteltettem.
Sok hibát csak az utóbbi módszerrel találtam meg.
A memóriakezelési hibák kiszűrésére a Valgrind programot és Czirkos Zoltán `debugmalloc` könyvtárát \(https://infoc.eet.bme.hu/debugmalloc/\) használtam.

## Futtatási környezet

A program az SDL könyvtárat használja a rajzoláshoz.
A program nem OS függő, elméletileg minden operációs rendszeren elfut, amire létezik az SDL könyvtár.

A program futásához szükséges a futtatási mappában lennie egy `font.ttf` filenak, amelynek valid TTF betűtípusnak kell lennie.
A javasolt (és mellékelt) betűtípus a Joystix.
A program a futtatási mappában lévő `palya.txt` szöveges file-ból fogja betölteni a pályát a specifikációnak megfelelően.
