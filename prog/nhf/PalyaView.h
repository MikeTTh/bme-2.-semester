//
// Created by mike on 28/04/2020.
//

#ifndef NHF_PALYAVIEW_H
#define NHF_PALYAVIEW_H
#include "View.h"
#include "Palya.h"

#include "memtrace.h"

/// @brief A pályát megjelenítő View.
class PalyaView: View {
public:
    void draw(Window&, Game&);
    void interact(SDL_Event*, Window&, Game&);
};


#endif //NHF_PALYAVIEW_H
