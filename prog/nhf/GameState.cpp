//
// Created by mike on 27/04/2020.
//
#include <sstream>

#include "GameState.h"
#include "TextBox.h"
#include "WinState.h"

#include "memtrace.h"

inline void finish(Game& g, State*& s) {
    s = new WinState(g);
}

void GameState::draw(Window& w, Game& g) {
    // Kirajzoljuk a pályát, ételt, és kígyókat
    palyaView.draw(w, g);
    foodView.draw(w, g);
    for (std::vector<Snake*>::const_iterator i = snakes.begin(); i != snakes.end(); ++i) {
        (*i)->draw(w, g);
    }
    
    int size = w.getY()/20;
    SDL_Rect rect = {0, 0, size, size};
    
    // Kiírjuk a kígyók pontszámait
    for (std::vector<Snake*>::const_iterator i = snakes.begin(); i != snakes.end(); ++i) {
        std::stringstream ss;
        ss << (*i)->score;
        
        TextBox tb(ss.str(), (*i)->color, rect);
        tb.draw(w, g);
        
        rect.x=2*size;
    }
    
    // Multiplayerben a kirajzoljuk a ":"-ot a két pontszám közt
    if (g.gm.multiplayer) {
        rect.x = size;
        colon.setPos(rect);
        colon.draw(w, g);
    }
    
    // Ha nem végtelen a játék akkor kirajzoljuk a "/<max>"-ot is
    if (!g.gm.endless) {
        rect.x = 0;
        rect.y = size;
        rect.w = 3*size;
        maxT.setPos(rect);
        maxT.draw(w, g);
    
        rect.x = 3*size;
        rect.w = size;
        std::stringstream ss;
        ss << g.p.getMaxScore();
        TextBox tb(ss.str(), (SDL_Color){255, 255, 255, 255}, rect);
        tb.draw(w, g);
    }
    
    // Ellenőrizzük, hogy valamelyik kígyó nyert-e
    if (!g.gm.endless) {
        int win = 0;
        for (std::vector<Snake*>::iterator i = snakes.begin(); i != snakes.end(); ++i) {
            if ((*i)->score >= g.p.getMaxScore()) {
                g.winner = win;
                finish(g, next);
                return;
            }
            win++;
        }
    }
    
    // 1. kígyó falba ütközik
    if (g.p.crashes(snakes[0]->getHeadPos())){
        g.winner = 1;
        finish(g, next);
        return;
    }
    // 1. kígyó önmagával ütközik
    if (snakes[0]->crashes(snakes[0]->getHeadPos())){
        g.winner = 1;
        finish(g, next);
        return;
    }
    
    if (g.gm.multiplayer) {
        // 2. kígyó falba ütközik
        if (g.p.crashes(snakes[1]->getHeadPos())) {
            g.winner = 0;
            finish(g, next);
            return;
        }
        // 2. kígyó magába ütközik
        if (snakes[1]->crashes(snakes[1]->getHeadPos())){
            g.winner = 1;
            finish(g, next);
            return;
        }
        
        // 1. kígyó a 2. kígyóba ütközik
        if (snakes[1]->crashes(snakes[0]->getHeadPos())){
            g.winner = 1;
            finish(g, next);
            return;
        }
        // és fordítva
        if (snakes[0]->crashes(snakes[1]->getHeadPos())) {
            g.winner = 0;
            finish(g, next);
            return;
        }
    }
}

void GameState::interact(SDL_Event* ev, Window& w, Game& g) {
    for (std::vector<Snake*>::iterator i = snakes.begin(); i != snakes.end(); ++i) {
        (*i)->interact(ev, w, g);
    }
}

GameState::GameState(Game& g): State(this), colon(":", (SDL_Color){255, 255, 255, 255}, (SDL_Rect){0, 0, 0, 0}), maxT("Max:", (SDL_Color){255, 255, 255, 255}, (SDL_Rect){0, 0, 0, 0}) {
    size_t size = 1;
    if (g.gm.multiplayer)
        size = 2;
    snakes.resize(size);
    for (size_t i = 0; i < size; ++i) {
        snakes[i] = new Snake(Snake::colors[i], Snake::cntrls[i], g.p.getStartPos(i));
    }    
}

GameState::~GameState() {
    for (std::vector<Snake*>::iterator i = snakes.begin(); i != snakes.end(); ++i) {
        delete (*i);
    }
}
