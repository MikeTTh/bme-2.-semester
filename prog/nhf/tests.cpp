//
// Created by mike on 10/05/2020.
//


#ifdef MEMTRACE
#include <exception>

#include "gtest_lite.h"

#include "SDL.h"
#include "Window.h"
#include "Snake.h"
#include "State.h"
#include "MenuState.h"
#include "GameState.h"
#include "WinState.h"

extern const char* testPalyaStr;

int main() {
    GTINIT(std::cin);
    
    TEST(Window, Sanity) {
        EXPECT_NO_THROW(Window w);
        FakeSDLMakeError(true);
        EXPECT_THROW(Window w, std::logic_error&);
        FakeSDLMakeError(false);
    } ENDM
    
    Window w;
    
    TEST(Window, Sanity2) {
        EXPECT_NO_THROW(w.getRenderer());
        EXPECT_NO_THROW(w.getFont());
    } ENDM
    
    TEST(Window, Resolution) {
        EXPECT_EQ(100, w.getX());
        EXPECT_EQ(200, w.getY());
    } ENDM
    
    TEST(Window, Unit) {
        w.Resize(100, 200);
        EXPECT_DOUBLE_EQ(w.getUnit(), 1.0);
    } ENDM
    
    TEST(Palya, Sanity) {
        EXPECT_NO_THROW(Palya p);
    } ENDM
    
    TEST(Palya, Empty) {
        Palya p;
        EXPECT_EQ((unsigned int)60, p.getX());
        EXPECT_EQ((unsigned int)40, p.getY());
    } ENDM
    
    TEST(Palya, LoadSanity) {
        std::stringstream testPalya(testPalyaStr);
        EXPECT_NO_THROW(Palya p(testPalya));
    } ENDM
    
    std::stringstream testPalya(testPalyaStr);
    Palya p(testPalya);
    
    TEST(Palya, Load) {
        EXPECT_EQ((unsigned int)12, p.getMaxScore());
        EXPECT_EQ((unsigned int)0, p.getStartPos(0).x);
        EXPECT_EQ((unsigned int)3, p.getStartPos(0).y);
        EXPECT_EQ((unsigned int)0, p.getStartPos(1).x);
        EXPECT_EQ((unsigned int)5, p.getStartPos(1).y);
        EXPECT_EQ(Palya::open, p(0,0));
        EXPECT_EQ(Palya::block, p(15,1));
        EXPECT_EQ(Palya::spawn, p(0,5));
    } ENDM
    
    TEST(Palya, Crashes) {
        Position pos(14.5, 1.5);
        EXPECT_EQ(true, p.crashes(pos));
    } ENDM
    
    
    TEST(Game, Sanity) {
        EXPECT_NO_THROW(Game g(p));
    } ENDM
    
    Game g(p);
    
    TEST(Game, Palya) {
        EXPECT_EQ((unsigned int)12, g.p.getMaxScore());
    } ENDM
    
    TEST(Game, FromFile) {
        Game filebol;
        EXPECT_EQ((unsigned int)72, filebol.p.getX());
        EXPECT_EQ((unsigned int)40, filebol.p.getY());
    } ENDM
    
    TEST(Snake, Sanity) {
        EXPECT_NO_THROW(Snake s);
        Snake s((SDL_Color){}, Snake::Controls(), Position());
    } ENDM
    
    
    TEST(Snake, Move) {
        Position start;
        Snake s(Snake::colors[0], Snake::cntrls[0], start);
        EXPECT_DOUBLE_EQ(start.x, s.getHeadPos().x);
        EXPECT_DOUBLE_EQ(start.y, s.getHeadPos().y);
        SDL_Event ev;
        ev.type = SDL_KEYDOWN;
        ev.key.keysym.scancode = Snake::cntrls[0].right;
        s.interact(&ev, w, g);
        FakeSDLRunTimers(100);
        EXPECT_DOUBLE_EQ(start.x + 1.5, s.getHeadPos().x);
        EXPECT_DOUBLE_EQ(start.y, s.getHeadPos().y);
        
        ev.key.keysym.scancode = Snake::cntrls[0].down;
        s.interact(&ev, w, g);
        FakeSDLRunTimers(100);
        EXPECT_DOUBLE_EQ(start.x + 1.5, s.getHeadPos().x);
        EXPECT_DOUBLE_EQ(start.y + 1.5, s.getHeadPos().y);
        
        ev.key.keysym.scancode = Snake::cntrls[0].left;
        s.interact(&ev, w, g);
        FakeSDLRunTimers(100);
        EXPECT_DOUBLE_EQ(start.x, s.getHeadPos().x);
        EXPECT_DOUBLE_EQ(start.y + 1.5, s.getHeadPos().y);
        
        ev.key.keysym.scancode = Snake::cntrls[0].up;
        s.interact(&ev, w, g);
        FakeSDLRunTimers(100);
        EXPECT_DOUBLE_EQ(start.x, s.getHeadPos().x);
        EXPECT_DOUBLE_EQ(start.y, s.getHeadPos().y);
    } ENDM;
    
    TEST(Snake, Food) {
        Snake s(Snake::colors[0], Snake::cntrls[0], Position());
        g.food = Position(3,0);
        EXPECT_EQ((unsigned int)0, s.score);
        SDL_Event ev;
        ev.type = SDL_KEYDOWN;
        ev.key.keysym.scancode = SDL_SCANCODE_D;
        s.interact(&ev, w, g);
        for (int i = 0; i < 8; ++i) {
            FakeSDLRunTimers(30);
            s.draw(w, g);
        }
        EXPECT_EQ((unsigned int)1, s.score);
    } ENDM
    
    
    TEST(MenuState, Sanity) {
        EXPECT_NO_THROW(MenuState().draw(w, g));
    } ENDM
    
    
    TEST(MenuState, GameOptions) {
        g.gm.endless = true;
        g.gm.multiplayer = true;
        MenuState m;
        m.draw(w, g);
        SDL_Event ev;
        ev.type = SDL_MOUSEBUTTONDOWN;
        
        ev.button.x = 5;
        ev.button.y = 48;
        m.interact(&ev, w, g);
        EXPECT_TRUE(g.gm.multiplayer == false);
        
        ev.button.x = 57;
        ev.button.y = 48;
        m.interact(&ev, w, g);
        EXPECT_TRUE(g.gm.multiplayer == true);
        
        ev.button.x = 5;
        ev.button.y = 99;
        m.interact(&ev, w, g);
        EXPECT_TRUE(g.gm.endless == false);
        
        ev.button.x = 57;
        ev.button.y = 99;
        m.interact(&ev, w, g);
        EXPECT_TRUE(g.gm.endless == true);
    } ENDM
    
    TEST(MenuState, NextState) {
        MenuState m;
        m.draw(w, g);
        SDL_Event ev;
        ev.type = SDL_MOUSEBUTTONDOWN;
        ev.button.x = 31;
        ev.button.y = 150;
        m.interact(&ev, w, g);
        
        EXPECT_TRUE(&m != m.getNext());
        delete m.getNext();
    } ENDM
    
    TEST(GameState, Sanity) {
        EXPECT_NO_THROW(GameState(g).draw(w, g));
    } ENDM
    
    TEST(GameState, CrashSnakes) {
        g.gm.endless = false;
        g.gm.multiplayer = true;
        GameState s(g);
        
        SDL_Event ev;
        ev.type = SDL_KEYDOWN;
        ev.key.keysym.scancode = SDL_SCANCODE_W;
        s.interact(&ev, w, g);
        ev.key.keysym.scancode = SDL_SCANCODE_DOWN;
        s.interact(&ev, w, g);
        for (int i = 0; i < 15; ++i) {
            FakeSDLRunTimers(10);
            s.draw(w, g);
            if (s.getNext() != &s)
                break;
        }
        
        EXPECT_TRUE( &s != s.getNext());
        delete s.getNext();
    } ENDM
    
    TEST(WinState, Sanity) {
        g.gm.multiplayer = false;
        g.winner = -1;
        EXPECT_NO_THROW(WinState(g).draw(w, g));
        g.winner = 0;
        EXPECT_NO_THROW(WinState(g).draw(w, g));
        g.gm.multiplayer = true;
        EXPECT_NO_THROW(WinState(g).draw(w, g));
        g.gm.multiplayer = false;
        EXPECT_NO_THROW(WinState(g).draw(w, g));
    } ENDM
    
    GTEND(std::cerr);
}

const char* testPalyaStr = "                                12\n"
                           "               X                  \n"
                           "                             XXXXX\n"
                           "S           \n"
                           " \n"
                           "S\n"
                           "                          comment";

#endif