//
// Created by mike on 24/04/2020.
//

#ifndef NHF_SNAKE_H
#define NHF_SNAKE_H
#include "View.h"
#include "Position.h"
#include "SDL.h"

#include "memtrace.h"

/// @brief A kígyók adatait tartalmazó és őket rajzoló osztály
class Snake: public View {
public:
    /// @brief A kígyók írányítására szolgáló gombokat tároló struct.
    struct Controls {
        SDL_Scancode up;
        SDL_Scancode down;
        SDL_Scancode left;
        SDL_Scancode right;
        explicit Controls(SDL_Scancode up = SDL_SCANCODE_W, SDL_Scancode down = SDL_SCANCODE_S, SDL_Scancode left = SDL_SCANCODE_A, SDL_Scancode right = SDL_SCANCODE_D);
    };
    /// @brief A két alapértelmezett Control
    const static Snake::Controls cntrls[2];
    /// @brief A két alapértelmezett szín
    const static SDL_Color colors[2];
private:
    /// @brief A kígyó 1 egységnégyzet méretű eleme, kétszeresen láncolt lista része
    class SnakeNode {
    public:
        /// @brief Látható-e az elem
        const bool visible;
        /// @brief Az elem pozíciója
        Position pos;
        /// @brief A következő elem
        SnakeNode* next;
        /// @brief Az előző elem
        SnakeNode* prev;
        /// @brief Az elemet kirajzoló fv.
        void draw(const SDL_Color*, Window&);
        /// @brief Konstruktor, ami az új node-ot be is fűzi a listába
        SnakeNode(SnakeNode* prev, Position pos, bool visible);
    };
    
    /// @brief A kígyót mozgató timer ID-jét tárolja
    SDL_TimerID timerId;
    
    /// @brief Az elemek listájának elejére mutató pointer, vagy szó szerint a kígyó feje.
    SnakeNode* head;
    /// @brief A kígyó utolsó elemére mutató pointer
    SnakeNode* tail;
    
    /// @brief Az adott kígyót vezérlő gombokat leíró Controls objektum
    Controls cntrl;
public:
    /// @brief A kígyó pontszáma
    unsigned int score;
    /// @brief A kígyó színe
    const SDL_Color color;
    /// @brief A kígyó jelenlegi haladási iránya
    Position direction;
    explicit Snake(SDL_Color = colors[0], Controls = cntrls[0], Position = Position());
    ~Snake();
    void draw(Window&, Game&);
    void interact(SDL_Event*, Window&, Game&);
    /// @brief A kígyó fejének pozícióját adja vissza.
    const Position& getHeadPos();
    /// @brief Ellenőrzi, hogy a megadott Position ütközik-e a kígyó bármely részével.
    bool crashes(const Position&) const;
    
    /**
     * @brief A kígyót mozgató timer függvénye
     * @param interval - Az SDL-től kapja, azt adja meg, hogy mennyi idő késleltetéssel lett meghívva
     * @param params - Az SDL miatt void*, ilyen paraméterű függvénypointert fogad csak el,
     * valójában ez mindig az adott kígyóra mutató pointerrel (this) van meghívva
     * @return Az SDL következőre a visszaadott értékkel (mint ms) késleltetve fogja meghívni újra a függvényt
     */
    static Uint32 timerFunc(Uint32 interval, void* params);
};

#endif //NHF_SNAKE_H
