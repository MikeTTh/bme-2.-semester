//
// Created by mike on 26/04/2020.
//

#include "FlashyText.h"
#include "TextBox.h"

#include "memtrace.h"

void FlashyText::draw(Window& w, Game& g) {
    int margin = w.getY() / 50;
    int rowH = (w.getY()-margin*3)/4;
    int cellW = (w.getX()-margin*5)/2;
    // Cím helye
    SDL_Rect draw = {w.getX()/2-cellW/2, margin, cellW, rowH};
    for (int i = 1; i < 10; ++i) { // Vibráló feliratok
        SDL_Rect rave = draw;
        rave.x += rand() % (w.getX() / 50) - w.getX() / 100;
        rave.y += rand() % (w.getY() / 50) - w.getY() / 100;
        
        tb[i]->setPos(rave);
        tb[i]->draw(w, g);
    }
    // Sima felirat
    tb[0]->setPos(draw);
    tb[0]->draw(w, g);
}

void FlashyText::interact(SDL_Event *, Window &, Game &) {}

FlashyText::FlashyText() {
    // Egy fehér a legfelső szövegnek
    tb[0] = new TextBox("Sznék", (SDL_Color){255, 255, 255, 255}, (SDL_Rect){0, 0, 0, 0});
    for (size_t i = 1; i < 20; ++i) {
        // A többi random színű
        tb[i] = new TextBox("Sznék", (SDL_Color){(Uint8)(rand() % 256), (Uint8)(rand() % 256), (Uint8)(rand() % 256), (Uint8)(rand() % 256)}, (SDL_Rect){0, 0, 0, 0});
    }
}

FlashyText::~FlashyText() {
    for (size_t i = 0; i < 20; ++i) {
        delete tb[i];
    }
}
