#ifndef MEMTRACE
#include "SDL.h"
#include "Window.h"
#include "Game.hpp"
#include "State.h"
#include "MenuState.h"
#include <string>
#include <cstring>
#include <cstdlib>

#include "memtrace.h"

int main(int argc, char *argv[]);

#ifdef _WIN32
// Enélkül a MinGW nem akarja Windowsra cross-compile-olni nekem a programot
int WinMain(){
    return main(0, NULL);
}
#endif

int main(int argc, char *argv[]) {
    // rand inicializálása
    srand(time(NULL)+clock()/CLOCKS_PER_SEC);
    
    Window w;
    Game g;
    State* s = new MenuState();
    
    w.Resize(g.p.getX(), g.p.getY());
    
#ifdef SHOWFPS
    char fpsS[10];
#endif
    
    while (s != NULL){ // Az event loop és annak azok a részei, amik mindig kellenek
        int start = SDL_GetTicks();
        
        // Előző render törlése
        SDL_SetRenderDrawColor(w.getRenderer(), 0,0,0,255);
        SDL_RenderClear(w.getRenderer());
        
        // Reagálunk az összes eventre, ami történt, hierarchikusan a View-ek közt
        SDL_Event ev;
        while(SDL_PollEvent(&ev)){
            if(ev.type == SDL_QUIT) { // Alt+F4-gyel vagy más, rendszertől jövő akcióval bezárjuk
                delete s;
                s = NULL;
                return 0;
            } else if (s != NULL) {
                s->interact(&ev, w, g);
            }
        }
        
        // Kirajzoljuk az aktív állapotot, ami elindítja a rajzolási láncot
        s->draw(w, g);
        
        // Állapotátmenetek megvalósítása
        if (s->getNext() != s) { // Ha nem önmagát adta vissza a state, mint következő állapot, akkor átmenet van
            State* next = s->getNext();
            delete s;
            s = next;
        }

#ifdef SHOWFPS
        // Ha a SHOWFPS definiálva van, akkor a bal felső sarokra kirajzoljuk a jelenlegi FPS-t
        // "csúnya" manuális C string, de gyors, és kényelmes és csak debugolásra van
        sprintf(fpsS, "%4.0f FPS", 1000.0/w.lastTick);
        TextBox fps(fpsS, (SDL_Color){255, 255, 255, 255}, (SDL_Rect){.x = 0, .y = 0, .w = 150, .h = 45});
        fps.draw(w, g);
#endif
        
        if (w.lastTick < 2)
            SDL_Delay(2-w.lastTick);
    
        w.lastTick = SDL_GetTicks()-start;
        SDL_RenderPresent(w.getRenderer());
    }

    return 0;
}
#endif