/**
 * \file View.h
 * View osztály deklarációja
 */

#ifndef NHF_VIEW_H
#define NHF_VIEW_H
#include "Window.h"
#include "Game.hpp"
#include "SDL.h"

#include "memtrace.h"

/**
 * @brief View absztrakt osztály.
 * Minden képernyőn megjeleníthető objektum egy View.
 */
class View {
public:
    
    /**
     * @brief Egy adott View kirajzolásáért felel.
     * @param w - az ablak, amire rajzol
     * @param g - a jelenlegi játék objektuma
     */
    virtual void draw(Window& w, Game& g) = 0;
    
    /**
     * @brief Ezen a függvényen keresztül tud egy View reagálni az eventekre.
     * @param ev - az event, amire reagál
     * @param w - az ablak, amire rajzolhat
     * @param g - a jelenlegi játék objektuma
     */
    virtual void interact(SDL_Event* ev, Window& w, Game& g) = 0;
    virtual ~View(){}
};


#endif //NHF_VIEW_H
