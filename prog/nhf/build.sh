#!/usr/bin/env bash

WIN=$PWD/build/win64
LIN=$PWD/build/lin64
GCCFLAGS="-O3 -Wall -I/usr/include/SDL2 -D_REENTRANT -L/usr/lib -pthread"

mkdir -p $WIN
mkdir -p $LIN

gcc -c main.c -o $LIN/main.o $GCCFLAGS
gcc -c logic.c -o $LIN/logic.o $GCCFLAGS
gcc -c rajzol.c -o $LIN/rajzol.o $GCCFLAGS
gcc -c snake_common.c -o $LIN/snake_common.o $GCCFLAGS
gcc -c eventloops.c -o $LIN/eventloops.o $GCCFLAGS
gcc $LIN/*.o -o $PWD/build/snake -lm -lSDL2 -lSDL2_image -lSDL2_gfx -lSDL2_ttf $GCCFLAGS

#x86_64-w64-mingw32-gcc -c main.c -o $WIN/main.o $GCCFLAGS
#x86_64-w64-mingw32-gcc -c logic.c -o $WIN/logic.o $GCCFLAGS
#x86_64-w64-mingw32-gcc -c rajzol.c -o $WIN/rajzol.o $GCCFLAGS
#x86_64-w64-mingw32-gcc -c snake_common.c -o $WIN/snake_common.o $GCCFLAGS
#x86_64-w64-mingw32-gcc -c eventloops.c -o $WIN/eventloops.o $GCCFLAGS
#x86_64-w64-mingw32-gcc -c menus.c -o $WIN/menus.o $GCCFLAGS
#x86_64-w64-mingw32-gcc $WIN/*.o -o $PWD/build/snake.exe -lm -lSDL2 -lSDL2_image -lSDL2_gfx -lSDL2_ttf $GCCFLAGS

cd docs || exit
pandoc userdoc.md -o ../userdoc.pdf
pandoc progdoc.md -o ../progdoc.pdf
pandoc spec.md -o ../spec.pdf
pandoc felkesz.md -o ../felkesz.pdf