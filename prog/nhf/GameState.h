//
// Created by mike on 27/04/2020.
//

#ifndef NHF_GAMESTATE_H
#define NHF_GAMESTATE_H
#include "State.h"
#include "Snake.h"
#include "FoodView.h"
#include "TextBox.h"
#include "PalyaView.h"
#include <vector>

#include "memtrace.h"

/// @brief A játék azon állapota, ahol játszani lehet.
class GameState: public State {
    /// @brief A kígyókat tároló vektor
    std::vector<Snake*> snakes;
    /// @brief Az ételt kirajzoló View.
    FoodView foodView;
    
    /// @brief Multiplayerben a 2 játékos pontszáma közti ":" megjelenítéséért felel.
    TextBox colon;
    /// @brief A maximum pontszám megjelenítéséért felel normál módban.
    TextBox maxT;
    /// @brief A pálya megjelenítéséért felel.
    PalyaView palyaView;
    
public:
    void draw(Window&, Game&);
    void interact(SDL_Event*, Window&, Game&);
    
    /**
     * @brief GameState konstruktor
     * @param g - A játékot fajtáját Game osztály.
     */
    explicit GameState(Game& g);
    ~GameState();
};


#endif //NHF_GAMESTATE_H
