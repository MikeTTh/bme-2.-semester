//
// Created by mike on 26/04/2020.
//

#ifndef NHF_TEXTBOX_H
#define NHF_TEXTBOX_H
#include "View.h"
#include <string>

#include "memtrace.h"

/// @brief Szöveget kirajzoló View.
class TextBox: public View {
    /// @brief A kirajzolt szöveg
    const std::string text;
    /// @brief A szöveg színe
    SDL_Color color;
    /// @brief A szöveg helye a képernyőn (a téglalapban arányosan átméretezve és középre igazítva lesz rajzolva a szöveg)
    SDL_Rect pos;
    /// @brief A surface, melyre a szöveget rendereltük
    SDL_Surface* surface;
    /// @brief A texture, amivé alakítottuk a surface-t, ezt másoljuk a képernyőre minden draw()-ban
    SDL_Texture* texture;
public:
    virtual void draw(Window&, Game&);
    virtual void interact(SDL_Event*, Window&, Game&);
    /**
     * @brief Konstruktor
     * @param n - a szöveg, amit ki akarunk rajzolni
     * @param c - a szín, amivel kirajzoljuk
     * @param p - a pozíció, ahova rajzoljuk
     */
    TextBox(const std::string& n, SDL_Color c, SDL_Rect p = (SDL_Rect){0, 0, 0, 0});
    /// @brief Be tudunk állítani új pozíciót, ahova rajzolunk.
    void setPos(const SDL_Rect& r);
    ~TextBox();
};

#endif //NHF_TEXTBOX_H
