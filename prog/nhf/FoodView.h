//
// Created by mike on 27/04/2020.
//

#ifndef NHF_FOODVIEW_H
#define NHF_FOODVIEW_H
#include "View.h"

#include "memtrace.h"

/// @brief A pályán lévő ételt kirajzoló View.
class FoodView: public View {
public:
    void draw(Window&, Game&);
    void interact(SDL_Event*, Window&, Game&);
};


#endif //NHF_FOODVIEW_H
