//
// Created by mike on 27/04/2020.
//

#ifndef NHF_SDL_H
#define NHF_SDL_H

#ifndef MEMTRACE
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#else
#include <cstdlib>
#define SDL_INIT_EVERYTHING 0

typedef char SDL_Renderer;
typedef char SDL_Texture;
typedef char SDL_Window;
typedef char TTF_Font;
typedef size_t SDL_TimerID;
typedef unsigned short int Uint8;
typedef unsigned long int Uint32;

typedef enum {
    SDL_SCANCODE_W,
    SDL_SCANCODE_A,
    SDL_SCANCODE_S,
    SDL_SCANCODE_D,
    SDL_SCANCODE_UP,
    SDL_SCANCODE_DOWN,
    SDL_SCANCODE_LEFT,
    SDL_SCANCODE_RIGHT,
} SDL_Scancode;

typedef enum {
    SDL_WINDOWPOS_CENTERED,
    SDL_WINDOW_FULLSCREEN_DESKTOP,
    SDL_RENDERER_ACCELERATED,
    SDL_RENDERER_PRESENTVSYNC,
} SDL_Misc;

typedef enum {
    SDL_LASTEVENT,
    SDL_KEYDOWN,
    SDL_MOUSEBUTTONDOWN,
    SDL_QUIT,
} SDL_EventType;

typedef struct {
    int x;
    int y;
    int w;
    int h;
} SDL_Rect;

typedef struct {
    int r;
    int g;
    int b;
    int a;
} SDL_Color;

typedef struct {
    int w;
    int h;
} SDL_Surface;

typedef struct {
    SDL_EventType type;
    struct {
        struct {
            SDL_Scancode scancode;
        } keysym;
    } key;
    struct {
        int x;
        int y;
    } button;
} SDL_Event;

typedef struct {
    int w;
    int h;
} SDL_DisplayMode;

void SDL_SetRenderDrawColor(SDL_Renderer*, int, int, int, int);
SDL_Window* SDL_CreateWindow(const char*, SDL_Misc, SDL_Misc, int, int, SDL_Misc);
SDL_Renderer* SDL_CreateRenderer(SDL_Window*, int, int);
void SDL_GetCurrentDisplayMode(int, SDL_DisplayMode*);
SDL_TimerID SDL_AddTimer(Uint32, Uint32(*)(Uint32, void*), void*);
int SDL_Init(int);
void SDL_Delay(int);
void SDL_RenderPresent(SDL_Renderer*);
int TTF_Init();
TTF_Font* TTF_OpenFont(const char*, int);
void SDL_RemoveTimer(SDL_TimerID);
void SDL_RenderClear(SDL_Renderer*);
void SDL_DestroyRenderer(SDL_Renderer*);
void TTF_CloseFont(TTF_Font*);
void SDL_FreeSurface(SDL_Surface*);
void SDL_DestroyTexture(SDL_Texture*);
void SDL_DestroyWindow(SDL_Window*);
void SDL_RenderDrawRect(SDL_Renderer*, SDL_Rect*);
void SDL_RenderFillRect(SDL_Renderer*, SDL_Rect*);
SDL_Surface* TTF_RenderUTF8_Blended(TTF_Font*, const char*, SDL_Color);
SDL_Texture* SDL_CreateTextureFromSurface(SDL_Renderer*, SDL_Surface*);
void SDL_RenderCopy(SDL_Renderer*, SDL_Texture*, SDL_Rect*, SDL_Rect*);
int SDL_GetTicks();
int SDL_PollEvent(SDL_Event*);
const char* SDL_GetError();
const char* TTF_GetError();
void SDL_Quit();

void FakeSDLMakeError(bool);
void FakeSDLRunTimers(Uint32 t);

#endif

#endif //NHF_SDL_H
