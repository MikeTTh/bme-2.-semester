//
// Created by mike on 24/04/2020.
//

#ifndef NHF_WINDOW_H
#define NHF_WINDOW_H
#include "SDL.h"
#include "Position.h"

#include "memtrace.h"

/// @brief Az ablakot és renderer-t leíró osztály.
class Window {
    SDL_Window* window;
    SDL_Renderer* renderer;
    TTF_Font* font;
    /// @brief Megadja, hogy 1 egység hány pixelnek felel meg. A Window::resize állítja be.
    double unit;
    /// @brief Az ablak szélessége pixelekben
    int x;
    /// @brief Az ablak magassága pixelekben
    int y;
    /// @brief priváttá tett (így letiltott) másoló konstruktor. Ablakra nem értelmezhető a másolás művelete, emiatt tiltott.
    Window(const Window&);
    /// @brief priváttá tett (így letiltott) operator=. Ablakra nem értelmezhető az értékadás művelete, emiatt tiltott.
    Window& operator=(const Window&);
public:
    unsigned int lastTick;
    /**
     * @brief Az adott ablakhoz tartozó renderer-t adja vissza.
     */
    SDL_Renderer* getRenderer() {
        return renderer;
    }
    /**
     * @brief Az adott ablakhoz tartozó betűtípust adja vissza.
     */
    TTF_Font* getFont() const {
        return font;
    }
    /**
     * @brief Megadja, hogy 1 egység hány pixelnek felel meg.
     */
    double getUnit() const {
        return unit;
    }
    /**
     * @brief A képernyő szélessége pixelekben.
     */
    int getX() const {
        return x;
    }
    /**
     * @brief A képernyő magassága pixelekben.
     */
    int getY() const {
        return y;
    }
    /**
     * @brief Kiszámolja a unit változó értékét megadott méretű pályához.
     * @param px - a pálya szélessége
     * @param py - a pálya magassága
     */
    void Resize(unsigned int px, unsigned int py);
    Window();
    ~Window();
};


#endif //NHF_WINDOW_H
