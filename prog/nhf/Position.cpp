//
// Created by mike on 24/04/2020.
//

#include "Position.h"
#include <cstdlib>

#include "memtrace.h"

bool Position::operator==(const Position& p) const {
    return x == p.x && y == p.y;
}

bool Position::operator<=(const Position& p) const {
    bool bal  = this->x <= p.x && this->x+1 >= p.x;
    bool jobb = this->x >= p.x && this->x <= p.x+1;
    bool fent = this->y <= p.y && this->y+1 >= p.y;
    bool lent = this->y >= p.y && this->y <= p.y+1;
    return (bal || jobb) && (fent || lent);
    /*
     * Van rövidebb collision detection is, de ezen jobban látszik, hogy mi történik,
     * és a compiler úgyis kioptimalizálja.
     * */
}


void Position::loopAround(unsigned int X, unsigned int Y) {
    if (x>X)
        x = x-X;
    if (y>Y)
        y = y-Y;
    if (x<0)
        x = X-x;
    if (y<0)
        y = Y-y;
}

void Position::randomize(unsigned int X, unsigned int Y) {
    x = rand() % X;
    y = rand() % Y;
}

Position::Position(double x, double y): x(x), y(y) {}

bool Position::operator>=(const Position &p) const {
    return this->operator<=(p);
}