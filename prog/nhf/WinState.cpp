//
// Created by mike on 27/04/2020.
//

#include "WinState.h"
#include <sstream>
#include "MenuState.h"

#include "memtrace.h"

/// @brief Új játékot indító functor
class newGameF{
    State*& next;
public:
    explicit newGameF(State*& n): next(n){}
    void operator()(){
        next = new MenuState();
    }
};

/// @brief A játékot bezáró functor
class closeF{
    State*& next;
public:
    explicit closeF(State*& n): next(n){}
    void operator()(){
        next = NULL;
    }
};

void WinState::draw(Window& w, Game& g) {
    if (firstdraw) {
        /*
         * Dinamikusan kiszámoljuk, hogy hova kéne kerülnie a UI elemeknek, a számokat
         * kísérletezéssel kaptam.
         * */
        
        int margin = w.getY() / 10;
        SDL_Rect r = {margin, margin, w.getX()-2*margin, w.getY()/6};
        gameOver.setPos(r);
    
        r.y += r.h+margin;
        r.h = w.getY()/8;
        winTxt->setPos(r);
    
        r.y = w.getY()-r.h-2*margin;
        r.x = margin;
        r.w = w.getX()/2 - 2*margin;
        r.h += margin;
        newGame->setPos(r);
    
        r.x += r.w + 2*margin;
        close->setPos(r);
        
        firstdraw = false;
    }
    
    gameOver.draw(w, g);
    winTxt->draw(w, g);
    newGame->draw(w, g);
    close->draw(w, g);
}

void WinState::interact(SDL_Event* ev, Window& w, Game& g) {
    newGame->interact(ev, w, g);
    close->interact(ev, w, g);
}

WinState::WinState(Game& g):
    State(this),
    gameOver("Játék vége", (SDL_Color){255, 255, 255, 255}),
    newGame(NULL),
    close(NULL),
    firstdraw(true) {
    std::stringstream txt;
    if (g.gm.multiplayer) {
        txt << "A";
        if (g.winner == 0) {
            txt << "z";
        }
        txt << " " << g.winner+1 << ". játékos nyert!";
    } else {
        if (g.winner == 0) {
            txt << "Nyertél!";
        } else {
            txt << "Vesztettél!";
        }
    }
    
    winTxt = new TextBox(txt.str(), (SDL_Color){255, 255, 255, 255}, (SDL_Rect){});
    newGame = new Button<newGameF>(newGameF(next), (SDL_Rect){}, "Új játék");
    close = new Button<closeF>(closeF(next), (SDL_Rect){}, "Bezárás", true);
}

WinState::~WinState() {
    delete newGame;
    delete close;
    delete winTxt;
}
