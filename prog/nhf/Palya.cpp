//
// Created by mike on 24/04/2020.
//

#include "Palya.h"
#include <iostream>
#include <fstream>

#include "memtrace.h"

Palya::Tile Palya::operator()(size_t X, size_t Y) const {
    return palya[X][Y];
}

Palya::Matrix::Matrix(size_t r, size_t c, const Tile &init) {
    mat.resize(r);
    for (size_t i = 0; i < mat.size(); ++i) {
        mat[i].resize(c, init);
    }
}

void Palya::load(std::istream& istream) {
        x = 0;
        y = 1;
        normalModeScore = 0;
    
        char c;
        unsigned int pos = 0;
        while (istream.get(c)) { // "Mérés"
            if (c == '\n') {
                y++;
                if (x < pos)
                    x = pos;
                pos = 0;
            } else {
                pos++;
            }
        }
        if (x < pos)
            x = pos;
    
        // Vissza az elejére
        istream.clear();
        istream.seekg(0, std::istream::beg);
    
        // Foglalás
        palya = Matrix(x, y);
        pos = 0;
        unsigned int posY = 0;
        
        while (istream.get(c)){ // Pálya tényleges feldolgozása
            if (c == '\n') {
                for (; pos < x; ++pos) {
                    palya[pos][posY] = open;
                }
                posY++;
                pos = 0;
            }else if(isdigit(c)){
                normalModeScore = normalModeScore * 10 + (c - '0');
            } else {
                switch (c){
                    default:
                        palya[pos][posY] = open;
                        break;
                    case 'X':
                        palya[pos][posY] = block;
                        break;
                    case 'S':
                        palya[pos][posY] = spawn;
                        break;
                }
                pos++;
            }
        }
}

bool Palya::crashes(const Position& p) const {
    for (size_t X = (p.x > 1 ? p.x-1 : 0); X < x && X <= p.x+1; ++X) { // x +- 1
        for (size_t Y = (p.y > 1 ? p.y-1 : 0); Y < y && Y <= p.y+1; ++Y) { // y +- 1
            if ( palya[X][Y] == block ){
                Position tmp(X, Y);
                if (p<=tmp)
                    return true;
            }
        }
    }
    return false;
}

Position Palya::getStartPos(size_t i) const {
    unsigned int spawnPoints = 0;
    for (size_t X = 0; X < x && spawnPoints <= i; ++X) {
        for (size_t Y = 0; Y < y && spawnPoints <= i; ++Y) {
            if (palya[X][Y] == spawn) {
                if (spawnPoints == i)
                    return Position(X, Y);
                spawnPoints++;
            }
        }
    }
    return Position();
}

Palya::Palya(const char *filename): palya(), x(0), y(0), normalModeScore(0) {
    std::ifstream file;
    file.open(filename);
    load(file);
    file.close();
}

Palya::Palya(std::istream& is): palya(), x(0), y(0), normalModeScore(0) {
    load(is);
}

Palya::Palya(): palya(), x(60), y(40), normalModeScore(15) {
    palya = Matrix(x, y);
}
