//
// Created by mike on 26/04/2020.
//

#include "TextBox.h"
#include <cstring>

#include "memtrace.h"

void TextBox::draw(Window& win, Game& g) {
    if (surface == NULL || texture == NULL) {
        // Text lerenderelése egy surface-re
        surface = TTF_RenderUTF8_Blended(win.getFont(), text.c_str(), color);
        texture = SDL_CreateTextureFromSurface(win.getRenderer(), surface);
    }
    
    // Megállapjuk,  hogy melyik irányban lógna ki a szöveg a dobozból először
    // és ahhoz megfelelő nagyítási/kicsinyítési szorzót kiszámoljuk
    double scaleX = (double)pos.w / (double)surface->w;
    double scaleY = (double)pos.h / (double)surface->h;
    bool alignToX = scaleX < scaleY;
    double scale = alignToX ? scaleX : scaleY;
    SDL_Rect d = {.x = pos.x, .y = pos.y};
    d.w = (int)(scale * (double)surface->w);
    d.h = (int)(scale * (double)surface->h);
    
    // A doboz közepére igazítjuk a szöveget
    if(alignToX){
        d.y += (int)(((double)pos.h - scale * (double)surface->h) / 2.0);
    }else{
        d.x += (int)(((double)pos.w - scale * (double)surface->w) / 2.0);
    }
    
    // Elvégezzük a rajzolást
    SDL_RenderCopy(win.getRenderer(), texture, NULL, &d);
}

void TextBox::interact(SDL_Event*, Window&, Game&) {}

TextBox::TextBox(const std::string& n, SDL_Color c, SDL_Rect p): text(n), color(c), pos(p), surface(NULL), texture(NULL) {}

void TextBox::setPos(const SDL_Rect &r) {
    pos = r;
}

TextBox::~TextBox() {
    // Felszabadítjuk a már többet nem használt texture-t és surface-t
    SDL_FreeSurface(surface);
    SDL_DestroyTexture(texture);
}
