//
// Created by mike on 27/04/2020.
//

#ifndef NHF_WINSTATE_H
#define NHF_WINSTATE_H
#include "State.h"
#include "TextBox.h"
#include "Button.hpp"

#include "memtrace.h"

/// @brief Az állapot, ami a játék megnyerésével vagy elvesztésével áll be.
class WinState: public State {
    /// @brief A "Játék vége" felirat
    TextBox gameOver;
    /// @brief Az "Új játék" gomb
    ButtonAbstr* newGame;
    /// @brief A "Bezárás" gomb
    ButtonAbstr* close;
    //// @brief A felirat, ami azt tartalmazza, hogy ki a nyertes.
    TextBox* winTxt;
    /// @brief Azt tárolja, hogy megtörtént-e az első rajzolás
    bool firstdraw;
public:
    explicit WinState(Game&);
    ~WinState();
    void draw(Window&, Game&);
    void interact(SDL_Event*, Window&, Game&);
};


#endif //NHF_WINSTATE_H
