//
// Created by mike on 26/04/2020.
//

#ifndef NHF_FLASHYTEXT_H
#define NHF_FLASHYTEXT_H
#include "View.h"
#include "Window.h"
#include "Game.hpp"
#include "TextBox.h"

#include "memtrace.h"

/// @brief A kezdőképernyőn lévő vibráló "SZNÉK" szöveget megjelenítő View.
class FlashyText: public View {
    /// @brief A 20 egymásra rétegzett TextBox pointereit tároló tömb
    TextBox* tb[20];
public:
    FlashyText();
    void draw(Window&, Game&);
    void interact(SDL_Event*, Window&, Game&);
    ~FlashyText();
};


#endif //NHF_FLASHYTEXT_H
