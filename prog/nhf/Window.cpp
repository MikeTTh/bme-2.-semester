//
// Created by mike on 24/04/2020.
//

#include "Window.h"
#include <cstdlib>
#include <ctime>
#include <stdexcept>

#include "memtrace.h"

Window::Window(): lastTick(-1) {
    // SDL inicializálása
    if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
        throw std::logic_error(SDL_GetError());
    
    SDL_DisplayMode dm;
    SDL_GetCurrentDisplayMode(0, &dm);
    x = dm.w;
    y = dm.h;
    
    unit = 0;
    
    // Teljes képernyős ablak létrehozása
    window = SDL_CreateWindow("Snake", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, x, y, SDL_WINDOW_FULLSCREEN_DESKTOP);
    if (window == NULL)
        throw std::logic_error(SDL_GetError());
    
    // GPU által gyorsított Vsync-re szabályozott renderer létrehozása
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL)
        throw std::logic_error(SDL_GetError());
    
    TTF_Init();
    
    font = TTF_OpenFont("font.ttf", 400);
    if (font == NULL)
        throw std::logic_error(TTF_GetError());
    
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);
}

Window::~Window() {
    TTF_CloseFont(font);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void Window::Resize(unsigned int px, unsigned int py) {
    unit = (double)y / py;
    if (px*unit > x)
        unit = (double)x / px;
}
