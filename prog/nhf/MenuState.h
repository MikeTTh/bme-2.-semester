//
// Created by mike on 26/04/2020.
//

#ifndef NHF_MENUSTATE_H
#define NHF_MENUSTATE_H

#include "State.h"
#include "Button.hpp"
#include "FlashyText.h"

#include "memtrace.h"

/// @brief A menü állapota. Itt lehet kiválasztani, hogy milyen játékot szeretnénk.
class MenuState: public State {
    /// @brief A vibráló "SZNÉK" felirat.
    FlashyText ft;
    /// @brief Az 5 gomb, ami a menüben van.
    ButtonAbstr* buttons[5];
    /// @brief Első rajzolás előtt true, utána false.
    bool firstDraw;
    
public:
    explicit MenuState();
    void draw(Window&, Game&);
    void interact(SDL_Event*, Window&, Game&);
    ~MenuState();
};


#endif //NHF_MENUSTATE_H
