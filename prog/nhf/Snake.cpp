//
// Created by mike on 24/04/2020.
//

#include "Snake.h"
#include "SDL.h"

#include "memtrace.h"

const double speed = 15.0; // egység/mp
const int sampleRate = 250.0; // Hz

const Snake::Controls Snake::cntrls[2] = {Snake::Controls(), Snake::Controls(SDL_SCANCODE_UP, SDL_SCANCODE_DOWN, SDL_SCANCODE_LEFT, SDL_SCANCODE_RIGHT)};
const SDL_Color Snake::colors[2] = {(SDL_Color){0, 255, 0, 255}, (SDL_Color){0, 0, 255, 0}};

Snake::SnakeNode::SnakeNode(SnakeNode *prev, Position pos, bool visible): visible(visible), pos(pos), prev(prev) {
    if (prev != NULL){
        next = prev->next;
        prev->next = this;
    } else {
        next = NULL;
    }
}

void Snake::SnakeNode::draw(const SDL_Color* c, Window& w) {
    if (visible) {
        SDL_Rect block;
        block.x = (int)(w.getUnit()*pos.x);
        block.y = (int)(w.getUnit()*pos.y);
        block.h = block.w = (int)w.getUnit();
        SDL_SetRenderDrawColor(w.getRenderer(), c->r, c->g, c->b, c->a);
        if(prev == NULL) // A kezdő node (fej) telt négyzet, többi nem
            SDL_RenderFillRect(w.getRenderer(), &block);
        else
            SDL_RenderDrawRect(w.getRenderer(), &block);
    }
}

void Snake::draw(Window& w, Game& g)  {
    for (SnakeNode* cur = head; cur != NULL; cur = cur->next) {
        cur->draw(&color, w);
    }
    
    head->pos.loopAround(g.p.getX(), g.p.getY());
    
    if (head->pos <= g.food){ // Felvette-e az ételt
        score++;
        g.food.randomize(g.p.getX(), g.p.getY());
        while (g.p.crashes(g.food))
            g.food.randomize(g.p.getX(), g.p.getY());
        
        /*
         * Annyi láthatatlan node-ot hozunk létre, ami az adott mozgással és mintavételezéssel
         * pont egy folytonos kígyót eredményez
         * */
        for (int j = 0; j < (int)(sampleRate/speed)-1; ++j) {
            tail = new SnakeNode(tail, tail->pos, false);
        }
        
        // Az utolsó látható
        tail = new SnakeNode(tail, tail->pos, true);
    }
}

Snake::Snake(SDL_Color c, Controls contr, Position start): cntrl(contr), score(0), color(c), direction(Position()) {
    head = tail = new SnakeNode(NULL, start, true);
    timerId = SDL_AddTimer(1000.0/sampleRate, timerFunc, this);
}

Snake::~Snake() {
    SDL_RemoveTimer(timerId);
    SnakeNode* current = head;
    SnakeNode* next = current;
    while (next != NULL) {
        next = current->next;
        delete current;
        current = next;
    }
}

void Snake::interact(SDL_Event* ev, Window&, Game& g) {
    if (ev->type == SDL_KEYDOWN) {
        if (ev->key.keysym.scancode == cntrl.up) {
            if (direction.y != 1)
                direction = Position(0, -1);
        } else if (ev->key.keysym.scancode == cntrl.down) {
            if (direction.y != -1)
                direction = Position(0, 1);
        } else if (ev->key.keysym.scancode == cntrl.left) {
            if (direction.x != 1)
                direction = Position(-1, 0);
        } else if (ev->key.keysym.scancode == cntrl.right) {
            if (direction.x != -1)
                direction = Position(1, 0);
        }
    }
}

Uint32 Snake::timerFunc(Uint32 interval, void *params) {
    /*
     * A mintavételezési sebességgel pakoljuk a kígyó elemeit
     * előre egyesével (láthatatlanokat is beleszámítva)
     * */
    Snake* snake = (Snake*)params;
    
    // A fejet mozgatjuk
    //               s  =                     v     *          t
    snake->head->pos.x += snake->direction.x*(speed * ((double)interval) / 1000.0);
    snake->head->pos.y += snake->direction.y*(speed * ((double)interval) / 1000.0);
    
    // A többi elemet mind eggyel előrébb pakoljuk (emiatt kellenek a láthatatlan node-ok)
    SnakeNode* node = snake->tail;
    while (node->prev != NULL){
        node->pos = node->prev->pos;
        node = node->prev;
    }
    return 1000.0/sampleRate;
}

const Position& Snake::getHeadPos() {
    return head->pos;
}

bool Snake::crashes(const Position& p) const {
    SnakeNode* tmp = head;
    // Ha saját magával ütköztetjük, akkor átugorjuk az első 2 látható elemet (másképp mindig igaz lenne)
    if  (&(head->pos) == &p) {
        for (size_t visibles = 0; visibles <= 2 && tmp != NULL; tmp = tmp->next){
            if (tmp->visible) {
                visibles++;
            }
        }
    }
    
    // Ütközések ellenőrzése
    for (; tmp != NULL; tmp = tmp->next) {
        if (tmp->visible && p <= tmp->pos) {
            return true;
        }
    }
    return false;
}

Snake::Controls::Controls(SDL_Scancode up, SDL_Scancode down, SDL_Scancode left, SDL_Scancode right): up(up), down(down), left(left), right(right) {}
