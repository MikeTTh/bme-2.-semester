//
// Created by mike on 26/04/2020.
//

#include "MenuState.h"
#include "Button.hpp"
#include "GameState.h"
#include <iostream>

#include "memtrace.h"

/// @brief Egyjátékos módot beállító functor
class singleF{
    Game& g;
public:
    explicit singleF(Game& g): g(g){}
    void operator()(){
        g.gm.multiplayer = false;
    }
};

/// @brief Kétjátékos módot beállító functor
class multiF{
    Game& g;
public:
    explicit multiF(Game& g): g(g){}
    void operator()(){
        g.gm.multiplayer = true;
    }
};

/// @brief Normál módot beállító functor
class normalF{
    Game& g;
public:
    explicit normalF(Game& g): g(g){}
    void operator()(){
        g.gm.endless = false;
    }
};

/// @brief Végtelen módot beállító functor
class endlessF{
    Game& g;
public:
    explicit endlessF(Game& g): g(g){}
    void operator()(){
        g.gm.endless = true;
    }
};

/// @brief Játékot indító functor
class startF {
    State*& s;
    Game& g;
public:
    startF(State*&s, Game& g): s(s), g(g) {}
    void operator()(){
        s = new GameState(g);
    }
};

MenuState::MenuState(): State(this), firstDraw(true) {}

void MenuState::draw(Window& w, Game& g) {
    ft.draw(w, g);
    
    if (firstDraw) {
        /*
         * Azért nem konstruktorban inicializáljuk ezeket a mezőket, hanem első foglaláskor,
         * hogy ne kelljen a konstruktornak lepasszolni a Game-et.
         *
         * Mérhető sebességbeli különbséget nem okoz.
         * */
        buttons[0] = new Button<singleF>(singleF(g), (SDL_Rect){}, "Egyjátékos", true);
        buttons[1] = new Button<multiF>(multiF(g), (SDL_Rect){}, "Kétjátékos");
        buttons[2] = new Button<normalF>(normalF(g), (SDL_Rect){}, "Normál", true);
        buttons[3] = new Button<endlessF>(endlessF(g), (SDL_Rect){}, "Végtelen");
        buttons[4] = new Button<startF>(startF(next, g), (SDL_Rect){}, "Start");
        
        int margin = w.getY() / 50;
        int rowH = (w.getY()-margin*3)/4;
        int cellW = (w.getX()-margin*5)/2;
    
        SDL_Rect draw = (SDL_Rect){margin, rowH, cellW, rowH};
        buttons[0]->setPos(draw);
        buttons[0]->setSel(&g.gm.multiplayer);
    
        draw = (SDL_Rect){cellW+margin*4, rowH, cellW, rowH};
        buttons[1]->setPos(draw);
        buttons[1]->setSel(&g.gm.multiplayer);
    
        draw = (SDL_Rect){margin, rowH*2+margin, cellW, rowH};
        buttons[2]->setPos(draw);
        buttons[2]->setSel(&g.gm.endless);
    
        draw = (SDL_Rect){cellW+margin*4, rowH*2+margin, cellW, rowH};
        buttons[3]->setPos(draw);
        buttons[3]->setSel(&g.gm.endless);
    
        draw = (SDL_Rect){w.getX()/2-cellW/2, rowH*3+margin*2, cellW, rowH};
        buttons[4]->setPos(draw);
        firstDraw = false;
    }
    
    for (int i = 0; i < 5; ++i) {
        buttons[i]->draw(w, g);
    }
}

void MenuState::interact(SDL_Event* ev, Window& w, Game& g) {
    if (firstDraw)
        return;
    for (int i = 0; i < 5; ++i){
        buttons[i]->interact(ev, w, g);
    }
}

MenuState::~MenuState() {
    if (!firstDraw) {
        for (int i = 0; i < 5; ++i) {
            delete buttons[i];
        }
    }
}
