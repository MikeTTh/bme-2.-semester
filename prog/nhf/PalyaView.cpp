//
// Created by mike on 28/04/2020.
//

#include "PalyaView.h"

#include "memtrace.h"

void PalyaView::draw(Window& w, Game& g) {
    SDL_SetRenderDrawColor(w.getRenderer(), 255, 255, 255, 0);
    for (size_t x = 0; x < g.p.getX(); ++x) {
        for (size_t y = 0; y < g.p.getY(); ++y) {
            if (g.p(x, y) == Palya::block) {
                SDL_Rect r = {(int)(x*w.getUnit()), (int)(y*w.getUnit()), (int)w.getUnit()+1, (int)w.getUnit()+1};
                SDL_RenderFillRect(w.getRenderer(), &r);
            }
        }
    }
}

void PalyaView::interact(SDL_Event *, Window &, Game &) {}
