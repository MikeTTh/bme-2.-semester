//
// Created by mike on 24/04/2020.
//

#ifndef NHF_PALYA_H
#define NHF_PALYA_H

#include <cstdlib>
#include <vector>
#include <fstream>
#include "Position.h"

#include "memtrace.h"

/// @brief A pályát tároló osztály.
class Palya {
public:
    /// @brief A három dolog, ami lehet a pálya egy egységnyi négyzetén.
    enum Tile{
        /// @brief Üres hely.
        open,
        /// @brief Fal.
        block,
        /// @brief Kezdőpozíció.
        spawn
    };
private:
    /// @brief A Palya egységnégyzetei ebben a mátrixban ("vektorvektorban") vannak tárolva.
    class Matrix {
        /// @brief Az adatokat tároló Tile vektor vektor.
        std::vector<std::vector<Tile> > mat;
    public:
        /**
         * @brief Kontruktor.
         * @param r - rows, sorok száma
         * @param c - columns, oszlopok száma
         * @param init - kezdeti érték
         */
        explicit Matrix(size_t r = 0, size_t c = 0, const Tile& init = open);
        /// @brief indexelő operátor
        std::vector<Tile>& operator[](size_t i) {
            return mat[i];
        }
        /// @brief konstans indexelő operátor
        const std::vector<Tile>& operator[](size_t i) const {
            return mat[i];
        }
    };
    
    /// @brief A Palya egységnégyzeteinek tárolója.
    Matrix palya;
    /// @brief A pálya szélessége.
    unsigned int x;
    /// @brief A pálya magassága.
    unsigned int y;
    /// @brief Normál módban a győzelemhez kellő pontszám.
    unsigned int normalModeScore;
    /// @brief Streamből betöltés.
    void load(std::istream& istream);
public:
    /// @brief A pálya egy adott koordinátájában lévő Tile lekérése.
    Tile operator()(size_t x, size_t y) const;
    /// @brief Konstruktor filenévből.
    explicit Palya(const char* filename);
    /// @brief Konstruktor streamből.
    explicit Palya(std::istream& is);
    /// @brief default konstruktor, egy 15 pontos 60x40-es üres pályát hoz létre
    Palya();
    /// @brief A pálya szélességének lekérése.
    unsigned int getX() const {
        return x;
    }
    /// @brief A pálya magasságának lekérése.
    unsigned int getY() const {
        return y;
    }
    /// @brief A normál módban nyeréshez elegendő pontszám lekérése.
    unsigned int getMaxScore() const {
        return normalModeScore;
    }
    /**
     * @brief Ellenőrzi, hogy a megadott Position ütközik-e fallal.
     * @param p - Az ellenőrzött hely.
     * @return true, ha ütközik; false, ha nem
     */
    bool crashes(const Position& p) const;
    /// @brief Megadja az i. kígyó kezdőhelyét.
    Position getStartPos(size_t i) const;
};


#endif //NHF_PALYA_H
