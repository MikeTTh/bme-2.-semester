//
// Created by mike on 24/04/2020.
//

#ifndef NHF_GAME_HPP
#define NHF_GAME_HPP


#include "Position.h"
#include "Palya.h"

#include "memtrace.h"

/// @brief A játék adatait tároló osztály
class Game {
    /// @brief A játékmódot leíró struct.
    struct GameMode {
        bool multiplayer;
        bool endless;
    };
public:
    /// @brief A játékmód.
    GameMode gm;
    /// @brief Az étel pozíciója.
    Position food;
    /// @brief A nyertes vagy -1, ha nincs nyertes.
    int winner;
    /// @brief A jelenlegi pálya.
    Palya p;
    
    /**
     * @brief Konstruktor template, hogy mindenből lehessen Game-et csinálni, amiből Palya-t is.
     * @tparam T - A változó típúsa, amit a Palya konstruktora megkap
     * @param in - A változó, amit a Palya konstruktora megkap
     */
    template<typename T>
    explicit Game(T in): gm(), p(in) {
        gm.endless = true;
        gm.multiplayer = false;
        winner = -1;
        food.randomize(p.getX(), p.getY());
    }
    
    /// @brief Default ctor, alapesetben a pályát a program mappájában lévő palya.txt-ből töltjük be.
    Game(): gm(), p("palya.txt") {
        gm.endless = true;
        gm.multiplayer = false;
        winner = -1;
        food.randomize(p.getX(), p.getY());
    }
};


#endif //NHF_GAME_HPP
